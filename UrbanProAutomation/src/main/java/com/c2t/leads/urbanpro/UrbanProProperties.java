package com.c2t.leads.urbanpro;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class UrbanProProperties {

	public static Properties getProperties() {

		Properties prop = null;

		try {
			prop = new Properties();

			InputStream input = new FileInputStream("src/main/resources/com/c2t/leads/urbanpro/urbanpro.properties");
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {

		}

		return prop;
	}

	public static void main(String[] args) {
		getProperties();
	}

}
