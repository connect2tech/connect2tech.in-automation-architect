package com.anirban.bkup;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anirban.Models.Advertise;
import com.anirban.Models.driver;
import com.anirban.Models.properties;

public class UrbanProPOM_BAK {

	private By nextButton = By.linkText("Next");
	private By currentPage = By.className("currentStep");
	private By navigatePagesBar = By.cssSelector(".paginateButtonsnew.paddingTop20");

	private By advertiseLocator = By.cssSelector(".enquiry-card.clearfix");

	private By advertiseTitleLocator = By.tagName("h3");

	private By advertiseCategoryLocator = By.className("ec-category");

	private By customerDetailsLocator = By.className("ec-detail-block");
	private By customerDetailsChildLocator = By.tagName("p");
	
	private By locationPrefLocator=By.cssSelector(".mar-left.ec-detail-block");
	private By locationPrefChildLocator=By.tagName("p");

	private ArrayList<Advertise> AdvertiseData = new ArrayList<>();

	public ArrayList<Advertise> getAdvertiseData() {
		return AdvertiseData;
	}

	private WebDriverWait wait = new WebDriverWait(driver.instance,
			Integer.parseInt(properties.global.getProperty("EXP_TIME_OUT3")));

	// Click next button to nevigate next page
	public void nextButtonClick() {

		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		driver.instance.findElement(nextButton).click();
	}

	// Click on provided page number
	public void goToPage(String pageNumber) {

		wait.until(ExpectedConditions.elementToBeClickable(By.linkText(pageNumber)));
		driver.instance.findElement(By.linkText(pageNumber)).click();
	}

	// Return currently selected page number
	public String currentPageNumber() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(currentPage));
		return driver.instance.findElement(currentPage).getText();
	}

	// Return last page number for the website
	public String lastPageNumber() {

		WebElement navigateBarElem = driver.instance.findElement(navigatePagesBar);
		List<WebElement> list = navigateBarElem.findElements(By.tagName("a"));
		int size = list.size();

		return list.get(size - 2).getText();
	}

	// Traverse to the page no from where data collection to be started
	public void traverseToPage(int pageNumber) {

		if (pageNumber == 1) {
			// No need to traverse
		} else {
			if (pageNumber > 10) {
				goToPage("10");
				int i = 14;
				while (i < pageNumber) {
					goToPage(String.valueOf(i));
					i = i + 4;
				}
			}
			goToPage(String.valueOf(pageNumber));
		}

	}

	// Get data from UrbanPro pages

	public void addToAdvertiseData() {

		List<WebElement> advertises = driver.instance.findElements(advertiseLocator);
		for (WebElement currentAdvertise : advertises) {
			
			String title = currentAdvertise.findElement(advertiseTitleLocator).getText();
			
			String text = currentAdvertise.findElement(advertiseCategoryLocator).getText();
			String arr[]=text.split(" \\| ");
			String category= arr[0].split(" ",2)[1];
			String posted=arr[1].split(" ",2)[1];

			WebElement customer = currentAdvertise.findElement(customerDetailsLocator);
			List<WebElement> childs = customer.findElements(customerDetailsChildLocator);
			String name = childs.get(1).getText();
			String location = childs.get(2).getText();
			String availability = childs.get(3).getText();
			
			String locationPreference="";
			try {
				List<WebElement> locationPreferences=currentAdvertise.findElement(locationPrefLocator).findElements(locationPrefChildLocator);
				locationPreference+="1) "+locationPreferences.get(1).getText().trim()+". ";
				for(int i=2;i<locationPreferences.size();i++){
					locationPreference+=i+") "+locationPreferences.get(i).getText().trim()+". ";
				}
				
//				locationPreference=currentAdvertise.findElement(locationPrefLocator).getText();
//				locationPreference=locationPreference.replaceFirst("Location Preference", "").trim();
			} catch (Exception e) {
				// 
			}
			
			AdvertiseData.add(new Advertise(title, category, posted, name, location, availability, locationPreference));

		}
	}

}
