package com.anirban.Exceptions;

public class WrongIdentifierNameException extends Exception{
	
	public WrongIdentifierNameException(String errorMessage) {
		super(errorMessage);
	}

}
