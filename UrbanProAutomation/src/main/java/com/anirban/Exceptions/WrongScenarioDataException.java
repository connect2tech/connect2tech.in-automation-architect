package com.anirban.Exceptions;

public class WrongScenarioDataException extends Exception{
	
	public WrongScenarioDataException(String errorMessage) {
		super(errorMessage);
	}

}
