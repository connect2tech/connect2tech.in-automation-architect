package com.anirban.Models;

import java.io.IOException;
import java.util.Properties;

import com.anirban.DAO.ExcelDAO;
import com.anirban.Exceptions.WrongPropertiesNameException;

public class properties {

	public static Properties global;
	
	// Initialise
	public static void initGlobalProperties() {

		global = new Properties();
	}

	// Utility functions
	public static void configGlobalProperties(String configExcelFilePath, String sheetName)
			throws IOException, WrongPropertiesNameException {

		ExcelDAO.addVariableFromSheetToProperties(configExcelFilePath, sheetName, "VARIABLE_NAME", "VARIABLE_VALUE",
				"global");

	}

	// Release
	public static void releaseGlobalProperties() {
		if (!global.isEmpty()) {
			global.clear();
		}
	}

}
