package com.anirban.Models;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.anirban.Exceptions.WrongScenarioDataException;
/*
 * Driver instance to be used implemented using Singleton design
 * Author : Anirban Pal
 * Date   : 27.04.2018
 */

public class driver {

	public static WebDriver instance;
	
	public static void initBrowser(String browser) throws WrongScenarioDataException{
		
		if (browser.equals("Internet_Explorer")) {
			driver.initIEDriver();
		} else if (browser.equals("Mozila_Firefox")) {
			driver.initFirefoxDriver();
		} else if (browser.equals("Google_Chrome")) {
			driver.initChromeDriver();
		} else {
			throw new WrongScenarioDataException(
					browser + " is not configured.");
		}
	}

	public static void initFirefoxDriver() {

		String fireFoxDriverPath = properties.global.getProperty("DRIVER_PATH")+"geckodriver.exe";
		System.out.println(fireFoxDriverPath);
		System.setProperty("webdriver.firefox.marionette", fireFoxDriverPath);
		instance = new FirefoxDriver();

		instance.manage().window().maximize();
		instance.manage().deleteAllCookies();
		instance.manage().timeouts().pageLoadTimeout(Integer.parseInt(properties.global.getProperty("EXP_TIME_OUT5")), TimeUnit.SECONDS);
	}

	public static void initChromeDriver() {

		String chromeDriverPath = properties.global.getProperty("DRIVER_PATH")+"chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		instance = new ChromeDriver();

		instance.manage().window().maximize();
		instance.manage().deleteAllCookies();
		instance.manage().timeouts().pageLoadTimeout(Integer.parseInt(properties.global.getProperty("EXP_TIME_OUT5")), TimeUnit.SECONDS);
	}

	public static void initIEDriver() {

		String iEDriverPath = properties.global.getProperty("DRIVER_PATH")+"IEDriverServer.exe";
		System.setProperty("webdriver.ie.driver", iEDriverPath);
		instance = new InternetExplorerDriver();

		instance.manage().window().maximize();
		instance.manage().deleteAllCookies();
		instance.manage().timeouts().pageLoadTimeout(Integer.parseInt(properties.global.getProperty("EXP_TIME_OUT5")), TimeUnit.SECONDS);
	}

	public static void resetDriver() {
		if (instance != null) {
			instance.quit();
			instance = null;
		}
	}

}
