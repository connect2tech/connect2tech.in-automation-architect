package com.anirban.Models;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	
	public static ExtentReports report;
	public static ExtentTest testInfo;
	public static ExtentHtmlReporter htmlReporter;
	
	private final static List statusHierarchy = Arrays.asList(
            Status.FATAL,
            Status.FAIL,
            Status.ERROR,
            Status.WARNING,
            Status.SKIP,
            Status.PASS,
            Status.DEBUG,
            Status.INFO
);
	
	//Initialize extentreport
	public static void initExtentReport(){
		
		htmlReporter=new ExtentHtmlReporter(new File(properties.global.getProperty("RESULT_INSTANCE_FOLDER_PATH")+"/ExtentReport.html"));
		htmlReporter.loadXMLConfig(new File(properties.global.getProperty("RESOURCE_PATH")+"extent-config.xml"));
		
		report=new ExtentReports();
		report.setSystemInfo("Environment", "PRODUCTION");
		report.setSystemInfo("Author", "Anirban Pal");
		report.setSystemInfo("System ", System.getProperty("os.name"));
		report.attachReporter(htmlReporter);
		
		//report.config().statusConfigurator().setStatusHierarchy(statusHierarchy);
		
	}
	
	public static void createExtenetReport(){
		if(report!=null){
			report.flush();
		}	
	}
	
	public static void createTestCase(String testName){
		testInfo=report.createTest(testName);
	}
	
	public static void addTestLog(String logType,String details){
		switch (logType) {
		case "INFO" : 
			testInfo.log(Status.INFO, details);
			break;
		case "DEBUG" : 
			testInfo.log(Status.DEBUG, details);
			break;
		case "ERROR" : 
			testInfo.log(Status.ERROR, details);
			break;
		case "FAIL" : 
			testInfo.log(Status.FAIL, details);
			break;
		case "FATAL" : 
			testInfo.log(Status.FATAL, details);
			break;
		case "PASS" : 
			testInfo.log(Status.PASS, details);
			break;
		case "SKIP" : 
			testInfo.log(Status.SKIP, details);
			break;
		case "WARNING" : 
			testInfo.log(Status.WARNING, details);
			break;
		case "ADD_SCREENSHOT" : 
			try {
				testInfo.addScreenCaptureFromPath(details);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		default:
			break;
		} 
	}
	
	

}
