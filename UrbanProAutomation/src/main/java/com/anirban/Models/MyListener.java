package com.anirban.Models;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.anirban.DAO.FilesDAO;

public class MyListener implements ITestListener {

	@Override
	public void onFinish(ITestContext context) {

	}

	@Override
	public void onStart(ITestContext context) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		

	}

	@Override
	public void onTestFailure(ITestResult result) {
		
		ExtentReport.addTestLog("FAIL", result.getName()+" execution failed.");	
		if(result.getThrowable()!=null){
			ExtentReport.addTestLog("ERROR", result.getThrowable().getMessage());	
		}
		
		File screenShot = ((TakesScreenshot) driver.instance).getScreenshotAs((OutputType.FILE));
		long time = new Date().getTime();
		String screenshotPath=properties.global.getProperty("RESULT_INSTANCE_FOLDER_PATH") +"\\"+ time + ".jpg";

		try {
			logger.instance.info("Instance result folder creation result : "
					+ FilesDAO.createFolder(properties.global.getProperty("RESULT_INSTANCE_FOLDER_PATH")));
			FileUtils.copyFile(screenShot,
					new File(screenshotPath));
			
			ExtentReport.addTestLog("ADD_SCREENSHOT", screenshotPath);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		

	}

	@Override
	public void onTestStart(ITestResult result) {
		ExtentReport.createTestCase(result.getName());
		ExtentReport.addTestLog("INFO", result.getName()+" execution started.");
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		ExtentReport.addTestLog("PASS", result.getName()+" execution completed.");		

	}

}
