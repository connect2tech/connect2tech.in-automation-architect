package com.anirban.POM;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.anirban.Models.Advertise;
import com.anirban.Models.driver;
import com.anirban.Models.properties;

public class UrbanProPOM {

	@FindBy(how = How.LINK_TEXT, using = "Next")
	private WebElement nextButtonElem;

	@FindBy(how = How.CLASS_NAME, using = "currentStep")
	private WebElement currentPageElem;

	@FindBy(how = How.CSS, using = ".paginateButtonsnew.paddingTop20>a")
	private List<WebElement> navigatePagesBarElems;

	@FindBy(how = How.CSS, using = ".enquiry-card.clearfix")
	private List<WebElement> advertiseElems;

	private By advertiseTitleLocator = By.tagName("h3");
	private By advertiseCategoryLocator = By.className("ec-category");
	private By customerDetailsLocator = By.className("ec-detail-block>p");
	private By locationPrefLocator = By.cssSelector(".mar-left.ec-detail-block>p");
	
	private ArrayList<Advertise> AdvertiseData = new ArrayList<>();

	private WebDriverWait wait = new WebDriverWait(driver.instance,
			Integer.parseInt(properties.global.getProperty("EXP_TIME_OUT3")));

	public UrbanProPOM() {
		PageFactory.initElements(driver.instance, this);
	}

	public ArrayList<Advertise> getAdvertiseData() {
		return AdvertiseData;
	}

	// Click next button to nevigate next page
	public void nextButtonClick() {

		wait.until(ExpectedConditions.elementToBeClickable(nextButtonElem));
		nextButtonElem.click();
	}

	// Click on provided page number
	public void goToPage(String pageNumber) {

		wait.until(ExpectedConditions.elementToBeClickable(By.linkText(pageNumber)));
		driver.instance.findElement(By.linkText(pageNumber)).click();
	}

	// Return currently selected page number
	public String currentPageNumber() {
		wait.until(ExpectedConditions.visibilityOf(currentPageElem));
		return currentPageElem.getText();
	}

	// Return last page number for the website
	public String lastPageNumber() {

		int size = navigatePagesBarElems.size();
		return navigatePagesBarElems.get(size - 2).getText();
	}

	// Traverse to the page no from where data collection to be started
	public void traverseToPage(int pageNumber) {
		if (pageNumber == 1) {
			// No need to traverse
		} else {
			if (pageNumber > 10) {
				goToPage("10");
				int i = 14;
				while (i < pageNumber) {
					goToPage(String.valueOf(i));
					i = i + 4;
				}
			}
			goToPage(String.valueOf(pageNumber));
		}
	}

	// Get data from UrbanPro pages
	public void addToAdvertiseData() {

		int i = 0;
		while (i < advertiseElems.size()) {
			String title = advertiseElems.get(i).findElement(advertiseTitleLocator).getText();

			String text = advertiseElems.get(i).findElement(advertiseCategoryLocator).getText();
			String arr[] = text.split(" \\| ");
			String category = arr[0].split(" ", 2)[1];
			String posted = arr[1].split(" ", 2)[1];

			String name = advertiseElems.get(i).findElements(customerDetailsLocator).get(1).getText();
			String location = advertiseElems.get(i).findElements(customerDetailsLocator).get(2).getText();
			String availability = advertiseElems.get(i).findElements(customerDetailsLocator).get(3).getText();

			String locationPreference = "";
			try {
				int size = advertiseElems.get(i).findElements(locationPrefLocator).size();
				if(size>1){
					for (int j = 1; j < size-1; j++) {
						locationPreference += j + ") "
								+ advertiseElems.get(i).findElements(locationPrefLocator).get(j).getText().trim() + ".\n";
					}
				}
				locationPreference += (size-1) + ") "
						+ advertiseElems.get(i).findElements(locationPrefLocator).get(size-1).getText().trim() + ".";
			} catch (Exception e) {
				// No Location Preference
			}

			AdvertiseData.add(new Advertise(title, category, posted, name, location, availability, locationPreference));
			i++;
		}
	}

}
