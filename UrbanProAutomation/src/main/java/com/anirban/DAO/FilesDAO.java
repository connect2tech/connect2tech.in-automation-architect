package com.anirban.DAO;

import java.io.File;

public class FilesDAO {
	
	public static boolean createFolder(String path){
		
		File f=new File(path);
		if(!f.exists()){
			return f.mkdir();
		}
		
		return true;	
	}

}
