package com.anirban.DAO;

import com.anirban.Models.logger;
import com.anirban.Models.properties;
import com.anirban.POM.UrbanProPOM;

public class ValidatorDAO {
	
	public static boolean validateStartEndPageNumber(){
		
		UrbanProPOM page=new UrbanProPOM();
		int firstPageNum=Integer.parseInt(page.currentPageNumber());
		int lastPageNum=Integer.parseInt(page.lastPageNumber());
		logger.instance.debug("For UrbanPro.com , first page number : "+firstPageNum+" and last page number : "+lastPageNum);
		if(Integer.parseInt(properties.global.getProperty("START_INDEX"))<firstPageNum || Integer.parseInt(properties.global.getProperty("START_INDEX"))>lastPageNum){
			return false;
		}
		
		if(Integer.parseInt(properties.global.getProperty("START_INDEX"))>Integer.parseInt(properties.global.getProperty("END_INDEX"))){
			return false;
		}else if(Integer.parseInt(properties.global.getProperty("END_INDEX"))<firstPageNum || Integer.parseInt(properties.global.getProperty("END_INDEX"))>lastPageNum){
			return false;
		}
		
		return true;	
	}

}
