package com.anirban.Test;

/*
 *   Written by       : Anirban Pal   	
'*   Start Date       :	10th June, 2018
 */

import java.io.IOException;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.anirban.DAO.FilesDAO;
import com.anirban.DAO.ExcelDAO;
import com.anirban.DAO.ValidatorDAO;
import com.anirban.Exceptions.WrongPropertiesNameException;
import com.anirban.Exceptions.WrongScenarioDataException;
import com.anirban.Models.AdvertiseGroup;
import com.anirban.Models.ExtentReport;
import com.anirban.Models.driver;
import com.anirban.Models.logger;
import com.anirban.Models.properties;
import com.anirban.POM.UrbanProPOM;
import com.aventstack.extentreports.Status;
import com.anirban.Models.MyListener;

@Listeners(MyListener.class)
public class TestCases {

	@BeforeClass
	public void setUp() {

		ClassLoader loader = TestCases.class.getClassLoader();
		String temp = loader.getResource("com/anirban/Test/TestCases.class").toString();
		String temp1 = temp.substring(temp.indexOf('/') + 1);
		String temp2[] = temp1.split("target");
		
		String variablePath = temp2[0] + "src/main/resources/Variables.xlsx";

		try {
			// Set properties
			properties.initGlobalProperties();
			properties.global.put("RESULT_PATH", (temp2[0] + "Result/"));
			properties.global.put("DRIVER_PATH", (temp2[0] + "Drivers/"));
			properties.global.put("RESOURCE_PATH", (temp2[0] + "src/main/resources/"));
			properties.configGlobalProperties(variablePath, "Parameters");

			logger.instance.info(temp2[0]);
			logger.instance.info("properties are set.");
			logger.instance.debug("Properties set : " + properties.global);
			
			//Result folder instance
			Date today=new Date();
			String folderName="Result-"+today.getHours()+today.getMinutes()+today.getSeconds()+today.getMonth()+today.getYear();
			String folderPath=properties.global.getProperty("RESULT_PATH")+folderName;
			properties.global.put("RESULT_INSTANCE_FOLDER_PATH", folderPath);
			
			//Initialize extent report generation
			ExtentReport.initExtentReport();

			// Initialize browser
			driver.initBrowser(properties.global.getProperty("Browser"));
			logger.instance.info(properties.global.getProperty("Browser") + " browser Initialized");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (WrongPropertiesNameException e) {
			e.printStackTrace();
		} catch (WrongScenarioDataException e) {
			e.printStackTrace();
		}
	}

	@AfterClass()
	public void cleanUp() {

		ExtentReport.createExtenetReport();
		properties.releaseGlobalProperties();
		driver.resetDriver();

	}

	@Test(priority = 1)
	public void checkPageTitle() {
		driver.instance.get(properties.global.getProperty("URL"));

		Assert.assertEquals(driver.instance.getTitle(), properties.global.getProperty("Title"));
		logger.instance.info("URL opened. Title - " + driver.instance.getTitle() + " Current URL - "
				+ driver.instance.getCurrentUrl());
		
		ExtentReport.addTestLog("INFO","URL opened. Title - " + driver.instance.getTitle());
	}

	@Test(priority = 2)
	public void getData() {
		
		ExtentReport.addTestLog("INFO","Start Page No : "+properties.global.getProperty("START_INDEX"));
		ExtentReport.addTestLog("INFO","End Page No : "+properties.global.getProperty("END_INDEX"));
		boolean result = ValidatorDAO.validateStartEndPageNumber();
		logger.instance.debug("Start End Index Validation Result : " + result);
		Assert.assertTrue(result);
		
		UrbanProPOM page = new UrbanProPOM();
		page.traverseToPage(Integer.parseInt(properties.global.getProperty("START_INDEX")));
		logger.instance.info("Traversed to page no : "+properties.global.getProperty("START_INDEX"));
		ExtentReport.addTestLog("INFO","Traversed to page no : "+properties.global.getProperty("START_INDEX"));
		
		for (int i = Integer.parseInt(properties.global.getProperty("START_INDEX")); i <= Integer
				.parseInt(properties.global.getProperty("END_INDEX")); i++) {

			page.addToAdvertiseData();
			if (i < Integer.parseInt(properties.global.getProperty("END_INDEX"))) {
				page.nextButtonClick();
			}
			
			logger.instance.info("Completed data collection from page no : "+i);
			ExtentReport.addTestLog("INFO","Completed data collection from page no : "+i);
		}

		logger.instance.info("Total no of Advertise data collected : "+page.getAdvertiseData().size());
		logger.instance.info(
				"Data retrieve completed from pages " + Integer.parseInt(properties.global.getProperty("START_INDEX"))
						+ " to " + Integer.parseInt(properties.global.getProperty("END_INDEX")));

		AdvertiseGroup groupedAdvertiseData = new AdvertiseGroup(page.getAdvertiseData());
		
		//groupedAdvertiseData.display();

		logger.instance.info("Instance result folder creation result : "+FilesDAO.createFolder(properties.global.getProperty("RESULT_INSTANCE_FOLDER_PATH")));
		String advertiseDataPath=properties.global.getProperty("RESULT_INSTANCE_FOLDER_PATH")+"/AdvertiseData.xlsx";
		
		groupedAdvertiseData.getCollection().forEach((key,value)->{
			try {
				ExcelDAO.createOrUpdateExcelUPA(advertiseDataPath, key, value);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		logger.instance.info("AdvertiseData file created at path : "+advertiseDataPath);
		ExtentReport.testInfo.log(Status.PASS,"AdvertiseData file created at path : "+advertiseDataPath);
	}
}
