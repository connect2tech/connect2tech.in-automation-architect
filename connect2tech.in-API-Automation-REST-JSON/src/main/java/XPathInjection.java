
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class XPathInjection {
	public static void main(String[] args) {
		 String userName = "nc";
		String xpathQuery = "//user[name/text()='" + "lol' or 1=1 or 'a'='a" + "' And password/text()='"
				+ "password" + "']";
		System.out.println(xpathQuery);
	}
}
