
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SQLInjection {
	public static void main(String[] args) {
		//String userName = "nc";
		String userName = "' or '1'='1";
		String statement = "SELECT * FROM `users` WHERE `name` = '" + userName + "';";
		System.out.println(statement);
	}
}
