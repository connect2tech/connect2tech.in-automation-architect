Feature: Login Into StackOverFlow 
	Existing user should be able to login to account using correct credentials

Scenario: Login into account with correct credentials 
	Given User Naviagates to StackOverFlow Website 
	And User Clicks on Login Button on Home Page 
	And User Enters Valid username 
	And User Enters Valid password 
	When User click on login Button
	Then User is taken to successful login page