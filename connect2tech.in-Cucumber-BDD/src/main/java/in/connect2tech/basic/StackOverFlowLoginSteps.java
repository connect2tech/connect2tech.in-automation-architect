package in.connect2tech.basic;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StackOverFlowLoginSteps {
	@Given("^User Naviagates to StackOverFlow Website$")
	public void user_Naviagates_to_StackOverFlow_Website() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Given("^User Clicks on Login Button on Home Page$")
	public void user_Clicks_on_Login_Button_on_Home_Page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Given("^User Enters Valid username$")
	public void user_Enters_Valid_username() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Given("^User Enters Valid password$")
	public void user_Enters_Valid_password() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@When("^User click on login Button$")
	public void user_click_on_login_Button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}

	@Then("^User is taken to successful login page$")
	public void user_is_taken_to_successful_login_page() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}
}
