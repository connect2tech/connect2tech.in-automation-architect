package in.connect2tech.basic;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/in/connect2tech/basic", 
				glue = { "in.connect2tech.basic" }, 
				monochrome = true, plugin = { "pretty",
		"html:target/cucumber", "json:target/cucumber.json"})

public class TestRunner {
	public void a() {
	}
}
