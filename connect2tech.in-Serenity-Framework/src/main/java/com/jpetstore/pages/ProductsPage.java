package com.jpetstore.pages;

//Static Imports for Table
import static net.thucydides.core.matchers.BeanMatchers.the;
import static net.thucydides.core.pages.components.HtmlTable.inTable;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.jpetstore.utils.PetCategories;

import net.serenitybdd.core.annotations.findby.By;

public class ProductsPage extends BasePage {

	private static final String PRODUCT_TABLE = "//*[@id='Catalog']//table";
	
	private static final String ANCHOR = "//*[@id='Catalog']//table//td[normalize-space() = '";
	
	private static final String PROCEED_TO_CHECKOUT = "//a[contains(text(),'Proceed to Checkout')]";
	
	private static final String PRODUCTS_TABLE_LABEL = "//*[@id='Catalog']/h2";
	
	
	
	/**
	 * Select the pet by providing a name
	 * 
	 * @param petcategory petcategory
	 * @param petName petName
	 * @return ProductsPage
	 */
	public ProductsPage selectPetByName( PetCategories petcategory , String petName) {
		
		waitForTextToAppear( petcategory.getValue() );
		
		List<WebElement> rows = inTable( $(PRODUCT_TABLE) )
					.filterRows(the("Name" , equalTo(petName) ) );
		
		rows.get(0).findElement(By.tagName("a")).click();
		
		return this;
	}
	
	/**
	 * 
	 * This method will add a pet to the shopping cart
	 * 
	 * @param specificProductName specificProductName
	 * @return ProductsPage
	 */
	public ProductsPage addToCartSpecificProduct(String specificProductName) {
		
		$(PRODUCT_TABLE).shouldContainText(specificProductName);
		
		String addToCartP1 = "']//parent::tr//a[contains(@href,'addItemToCart')]";
		
		String addToCartButton =  ANCHOR + specificProductName + addToCartP1;
		
		System.out.println("addToCartButton------------->"+addToCartButton);
		
		waitFor(addToCartButton).$(addToCartButton).click();
		
		return this;
	}
	
	/**
	 * Add an item to the cart by viewing details of the pet
	 * 
	 * @param specificProduct specificProduct
	 * @param details details
	 * @return ProductsPage
	 */
	public ProductsPage addToCartByViewingItemDetails( String specificProduct ,
				String...details ) {
		
		
		$(PRODUCT_TABLE).shouldContainText(specificProduct);
		
		String viewItemLinkP1 = "']//parent::tr//a[contains(@href,'viewItem')]";
		
		String viewItemLink =  ANCHOR +  specificProduct + viewItemLinkP1;
		
		waitFor(viewItemLink).$(viewItemLink).click();
		
		waitForAnyTextToAppear(details[0] , details [1]);
		
		$("//a[@class = 'Button']").click();
		
		return this;
	}
	
	/**
	 * Selecting a product from the search results
	 * 
	 * @param productName productName
	 * @return ProductsPage
	 */
	public ProductsPage selectProductFromSearchTable( String productName) {
		
		String searchResultP1 = "']//parent::tr//img//parent::a";
		
		String searchResultLink =  ANCHOR + productName + searchResultP1;
		
		System.out.println("searchResultLink------------>"+searchResultLink);
		
		waitFor(searchResultLink).$(searchResultLink).click();
		
		return this;
	}
	
	/**
	 * Interact with the shopping cart
	 * 
	 * @param productDescription productDescription
	 * @param quantity quantity
	 * @return ProductsPage
	 */
	public ProductsPage shoppingCart( String productDescription ,  int quantity) {
		
		String quantityP1 = "']//following-sibling::td/input";
		
		String totalCostP1 = "']//following-sibling::td[4]";
		
		String quantityInput =  ANCHOR + productDescription + quantityP1;
		
		String totalCostLabel =  ANCHOR + productDescription + totalCostP1;
		
		
		if(quantity > 0 ) {
			
			Float totalvalueBeforeUpdate = Float.valueOf( $(totalCostLabel).getText().substring(1) );
			
			$(quantityInput).typeAndEnter(Integer.toString(quantity));
			
			Float totalvalueAfterUpdate = Float.valueOf( $(totalCostLabel).getText().substring(1) );
			
			if(totalvalueAfterUpdate < totalvalueBeforeUpdate ) {
				
				return null;
			}
		}
		
		if(quantity == 0 ) {
			
			$(quantityInput).typeAndEnter(Integer.toString(quantity));
			
			waitForAbsenceOf(quantityInput).shouldNotBeVisible(By.xpath(quantityInput) );
		}
		
		return this;
	}
	
	/***
	 * 
	 * Remove item from shopping cart
	 * 
	 * @param productName productName
	 * @return boolean
	 */
	public boolean removeItemFromCart(String productName ) {
		
		String removeBtnP1 = "']//following-sibling::td[5]/a";
		
		String removeBtn =  ANCHOR + productName + removeBtnP1;
		
		waitFor(removeBtn).$(removeBtn).click();
		
		return (!waitForAbsenceOf(removeBtn).isElementVisible(By.xpath(removeBtn)));
		
	}
	
	/**
	 * Click on proceed to checkout
	 * 
	 * 
	 * @return OrdersPage
	 */
	public OrdersPage clickOnProceedToCheckout() {
		
		waitFor(PROCEED_TO_CHECKOUT).$(PROCEED_TO_CHECKOUT).click();
		
		return this.switchToPage(OrdersPage.class);
	}
	
}
