package com.jpetstore.pages;

public class EntryPage extends BasePage {

	private static final String ENTER_THE_STORE_LINK = "//a[@href='actions/Catalog.action']";

	public DashBoardPage enterPetStore() {

		open();

		waitForTextToAppear("Welcome to JPetStore 6");

		// By default it will use implicit wait, so wrap it wait so that it waits for
		// explicit wait.
		waitFor(ENTER_THE_STORE_LINK).$(ENTER_THE_STORE_LINK).click();

		return this.switchToPage(DashBoardPage.class);

	}

}
