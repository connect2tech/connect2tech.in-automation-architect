Feature: Organization data Display 

Scenario Outline: Cucumber4 Migration 

	Given A Branch "<User>" has authority to make order or return request 
	When User login into "<Application_Name>" 
	Then User Navigates to "<Page>" 
	And Validate The screen details "<Ordering_Bank_Number>" and "<Branch_Number>" and "<Branch_User_ID>" and "<User_Name>" 
	And Validate details against database 
	Examples: Too Short 
		|Application_Name|User|Page|Ordering_Bank_Number|Branch_Number|Branch_User_ID|User_Name|
		|Rahahuolto Application|User1|Euro_Note_Order|BranchBankNo-001 | BranchNo-001 |BranchUserId-001 |UserName-001|
		|Rahahuolto Application|User2|Euro_Note_Order|BranchBankNo-002 | BranchNo-002 |BranchUserId-002 |UserName-002|