package in.connect2tech.book.chapter.bdd;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ShoppingCartStepsDefinition {

	@Steps
	BuyerSteps buyer;

	@Given("^I have searched for 'docking station'$")
	public void i_have_searched_for_docking_station() {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();

		buyer.openUrl();
		buyer.typeSearchItemInSearchBox();

	}

	@Given("^I have selected a matching item$")
	public void i_have_selected_a_matching_item() {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
		// int a = 10/0;
		// System.out.println(a);
	}

	@When("^I add it to the cart$")
	public void i_add_it_to_the_cart() {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@Then("^the shipping cost should be included in the total price$")
	public void the_shipping_cost_should_be_included_in_the_total_price() {
		// Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}
}
