package in.connect2tech.webelements.editbox;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("/users/add")
public class EditBoxPage extends PageObject {

	@FindBy(id = "name")
	private WebElementFacade name;

	@FindBy(id = "email")
	private WebElementFacade email;

	public void _editBoxtest() {

		open();
		
		name.type("Naresh");
		email.type("password");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// username.typeAndTab("tomsmith");
		// password.typeAndEnter("SuperSecretPassword!");

	}

}
