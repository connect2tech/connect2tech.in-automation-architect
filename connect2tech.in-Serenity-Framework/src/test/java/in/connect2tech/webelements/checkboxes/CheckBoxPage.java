package in.connect2tech.webelements.checkboxes;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webelements.Checkbox;


@DefaultUrl("/users/add")
public class CheckBoxPage extends PageObject {

	
	@FindBy(id ="framework1")
	private WebElementFacade _chkBox1;
	
	@FindBy(id ="framework2")
	private WebElementFacade _chkBox2;
	
	
	public void selectCheckBox() throws InterruptedException {
		
		open();
		
		Checkbox checkBox = new Checkbox(_chkBox1);
		
	//	checkBox.setChecked(true);
		
		Checkbox checkBox2 = new Checkbox(_chkBox2);
		
	//	checkBox2.setChecked(false);
		
		
		System.out.println("checkBox.isChecked()------------>"+checkBox.isChecked());
		
		System.out.println("checkBox2.isChecked()------------>"+checkBox2.isChecked());
		
		Thread.sleep(4000);
		
	}
	
	public void selectCheckBox2() throws InterruptedException {
		
		open();
		setCheckbox(_chkBox1, true);
		Thread.sleep(4000);
		
	}

}
