package in.connect2tech.webelements.dropdowns;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.components.Dropdown;


@DefaultUrl("/users/add")
public class DropDownPage extends PageObject {

	@FindBy(id = "country")
	private WebElementFacade country;
	
	public void selectValue() throws InterruptedException {
		
		open();
	//	Dropdown.forWebElement(dropdownId).selectByValue("2"); 
		
	//	Dropdown.forWebElement(dropdownId).select("Option 1");
		
		selectFromDropdown(country, "China");
		
		System.out.println("getSelectedLabelFrom(country)------->"+getSelectedLabelFrom(country));
		
		System.out.println("getSelectedValueFrom(country)------->"+getSelectedValueFrom(country));
		
			
		Thread.sleep(5000);
	}
	
}
