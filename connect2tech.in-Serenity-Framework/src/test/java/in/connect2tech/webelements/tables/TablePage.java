package in.connect2tech.webelements.tables;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.annotations.findby.How;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.components.HtmlTable;

//Static Imports for Table
import static net.thucydides.core.matchers.BeanMatchers.*;
import static net.thucydides.core.pages.components.HtmlTable.*;
import static org.hamcrest.Matchers.*;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

@DefaultUrl("/htmlElements")
public class TablePage extends PageObject {

	@FindBy(how = How.ID, using = "mytable")
	protected WebElementFacade table;

	// Read the complete Table as a List of Key Value pairs
	public void readCompleteTable() throws InterruptedException {
		
		open();
		List<Map<Object, String>> tbl = 
				HtmlTable.rowsFrom(table);
		System.out.println("tbl---------------------------->"+tbl);
		
	}

	// Extract rows based on certain conditions
	public void extractFilteredRowsFromTable() throws InterruptedException {

		open();
		List<WebElement> rowValues = 
				filterRows(table, the("Email" , endsWith("earthlink.net") ) );
		
		for(WebElement e : rowValues ) {
			//System.out.println(e.getText() );
		}

	}

	// Extract All the Headings
	public void extractHeadings() throws InterruptedException {

		open();
		List<String> headings = inTable(table).getHeadings();
		for(String h : headings) {
		//	System.out.println(h);
		}
	}

	// Extract All the Rows(except heading)
	public void extractAllTheRows() throws InterruptedException {

		open();
		List<WebElement> rowElements = 
				inTable(table).getRowElements();
		
		for(WebElement e : rowElements ) {
		//	System.out.println(e.getText() );
		}
		
		inTable(table).getRowElements().stream()
			.forEach(e -> System.out.println(e.getText()));
		
	}

	// Extract based on conditions
	public void extractRowsBasedOnConditions() throws InterruptedException {

		open();
		inTable(table).getRowElementsWhere(the("First Name" , equalTo("Frank")) )
				.stream().forEach(e -> System.out.println(e.getText() ));
	}

	// Read Rows which donot have headings
	public void extractwithNoHeadings() throws InterruptedException {

		open();
		
		List<Map<Object, String>> tbl = 
		withColumns("Last Name" , "First Name" ,"Email" , "Due" , "Web Site" , "Mytest")
			.readRowsFrom(table);
		
		//System.out.println(tbl);
	}

	// Check on certain values inside the table
	public void assertOnTableElements() throws InterruptedException {

		open();
		inTable(table)
			.shouldHaveRowElementsWhere(the("First Name" , equalTo("Frank")));
		
		inTable(table)
			.shouldNotHaveRowElementsWhere(the("First Name" , equalTo("Franrpwwpk")));
	}

}
