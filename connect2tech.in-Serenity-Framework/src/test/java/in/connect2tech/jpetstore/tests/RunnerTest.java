package in.connect2tech.jpetstore.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import in.connect2tech.jpetstore.steps.BasePageSteps;
import in.connect2tech.jpetstore.steps.EntryPageSteps;
import in.connect2tech.jpetstore.steps.PetStoreSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class RunnerTest {

	@Managed
	WebDriver driver;
	
	@Steps
	BasePageSteps basePageSteps;
	
	@Steps
	EntryPageSteps entryPageSteps;
	
	@Test
	@Title("Navigate to Sign On Page")
	public void navigateToSignOnPage() throws InterruptedException {
		
		entryPageSteps.enterPetStore();
		basePageSteps.navigateToLoginPage2();
		/*shopper.doLogin("test", "test");
		shopper.navigateToProductCategory(PetCategories.DOGS);
		shopper.selectPetByName(PetCategories.DOGS, "Dalmation");
		shopper.addToCartSpecificProduct("Spotted Adult Female Dalmation");
		shopper.clickOnProceedToCheckout();*/
		
	}
	
}
