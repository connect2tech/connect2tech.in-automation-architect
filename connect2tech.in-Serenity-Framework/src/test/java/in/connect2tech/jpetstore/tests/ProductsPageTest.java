package in.connect2tech.jpetstore.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.steps.BasePageSteps;
import in.connect2tech.jpetstore.steps.DashboardPageSteps;
import in.connect2tech.jpetstore.steps.EntryPageSteps;
import in.connect2tech.jpetstore.steps.ProductsPageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class ProductsPageTest {

	@Managed
	WebDriver driver;

	@Steps
	EntryPageSteps entryPageSteps;

	@Steps
	DashboardPageSteps dashBoardPageSteps;

	@Steps
	ProductsPageSteps productsPageSteps;

	@Steps
	BasePageSteps basePageSteps;

	//@Test
	@Title("Choose Category From LeftMenu and Add to Cart")
	public void chooseCategoryFromLeftMenuAndAddToCart() {

		entryPageSteps.enterPetStore();
		dashBoardPageSteps.selectProductFromSideBar(PetCategories.BIRDS);
		productsPageSteps.selectPetByName(PetCategories.BIRDS, "Finch");
		productsPageSteps.addToCartByViewingItemDetails("Adult Male Finch", "Great stress reliever",
				"Adult Male Finch");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	@Title("Searching for Pet in Search Box")
	public void searchProduct() {

		entryPageSteps.enterPetStore();
		basePageSteps.searchForProduct("Bulldog");
		productsPageSteps.selectProductFromSearchTable("Bulldog");
		productsPageSteps.addToCartByViewingItemDetails("Male Adult Bulldog", "Friendly dog from England",
				"Male Adult Bulldog");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
