package in.connect2tech.jpetstore.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.steps.BasePageSteps;
import in.connect2tech.jpetstore.steps.DashboardPageSteps;
import in.connect2tech.jpetstore.steps.EntryPageSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class DashBoardPageTest {

	@Managed
	WebDriver driver;

	@Steps
	EntryPageSteps entryPageSteps;

	@Steps
	DashboardPageSteps dashBoardPageSteps;

	@Test
	@Title("Choose Category from Left Menu")
	public void chooseCategoryFromLeftMenu() {

		entryPageSteps.enterPetStore();
		dashBoardPageSteps.selectProductFromSideBar(PetCategories.BIRDS);
		
	}

}
