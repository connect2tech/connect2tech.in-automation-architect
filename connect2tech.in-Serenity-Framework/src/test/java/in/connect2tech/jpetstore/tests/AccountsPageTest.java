package in.connect2tech.jpetstore.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.github.javafaker.Faker;

import in.connect2tech.jpetstore.steps.AcountPageSteps;
import in.connect2tech.jpetstore.steps.BasePageSteps;
import in.connect2tech.jpetstore.steps.LoginPageSteps;
import in.connect2tech.jpetstore.steps.PetStoreSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class AccountsPageTest {
	
	@Managed
	WebDriver driver;
	
	@Steps
	PetStoreSteps shopper;
	
	@Steps
	BasePageSteps basePageSteps;
	
	@Steps
	LoginPageSteps loginPageSteps;
	
	@Steps
	AcountPageSteps acountPageSteps;
	
	
	@Test
	@Title("Add a new user to store & verify if the new user can login")
	public void addNewUserToStoreAndVerify() throws InterruptedException {
		
		Faker faker = new Faker();
		
		/*String userName = "test" + faker.number().randomNumber(10, false);
		String password = faker.internet().password();*/
		String userName = "test22";
		String password = userName;
		String repeatPassword = password;
		String firstName = faker.name().firstName();
		String lastName =  faker.name().lastName();
		String email =  faker.internet().emailAddress();
		String phoneNumber = faker.phoneNumber().cellPhone();
		String addr1 = faker.address().buildingNumber();
		String addr2 = faker.address().streetAddress();
		String city = faker.address().city();
		String state = faker.address().state();
		String zipCode = faker.address().zipCode();
		String country = faker.address().country();
		
		basePageSteps.navigateToLoginPage();
		
		loginPageSteps.navigateToRegistrationPage();
		
		acountPageSteps.addNewUserInformation(userName, password, repeatPassword);
		
		acountPageSteps.addAccountInformation(firstName, lastName, email, phoneNumber, addr1, 
				addr2, city, state, zipCode, country);
		
		acountPageSteps.addProfileInformation("english", "DOGS", true, true);
			
		acountPageSteps.clickSaveAccountInformation();
		
		loginPageSteps.doLogin(userName, password);
		
		String greetingMessage =  shopper.getGreetingMessage();
		
		assertEquals("Welcome " + firstName + "!", greetingMessage);
		
	}

}
