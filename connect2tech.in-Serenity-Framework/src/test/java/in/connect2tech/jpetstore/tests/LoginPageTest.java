package in.connect2tech.jpetstore.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import in.connect2tech.jpetstore.steps.BasePageSteps;
import in.connect2tech.jpetstore.steps.DashboardPageSteps;
import in.connect2tech.jpetstore.steps.LoginPageSteps;
import in.connect2tech.jpetstore.steps.PetStoreSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class LoginPageTest {

	@Managed
	WebDriver driver;

	@Steps
	BasePageSteps basePageSteps;

	@Steps
	LoginPageSteps loginPageSteps;

	@Steps
	DashboardPageSteps dashboardPageSteps;

	/*@Test
	@Title("Verify if a user can login succesfully to the store with valid credentials")
	public void verifyIfLoginIsSuccessful() {

		basePageSteps.navigateToLoginPage();

		loginPageSteps.doLogin("test", "test");

		String greetingMessage = dashboardPageSteps.getGreetingMessage();

		assertEquals("Welcome Naresh!", greetingMessage);
	}

	@Test
	@Title("Verify if the user can signout successfully")
	public void verifyIfUserCanLogoutSuccesfully() {

		basePageSteps.navigateToLoginPage();

		loginPageSteps.doLogin("test", "test");

		basePageSteps.signOut();

	}*/

	@Test
	@Title("Verify if message <b><i> 'Invalid username or password. Signon failed'.</i></b> is displayed for "
			+ " invalid credentials")
	public void verifyIfMessageIsDisplayedFOrInValidLogin() {

		basePageSteps.navigateToLoginPage();

		/*loginPageSteps.doLogin("test", "testjsjdjd");

		String message = loginPageSteps.getMessageOnInvalidLogin();

		assertEquals("Invalid username or password. Signon failed.", message);*/
	}

}
