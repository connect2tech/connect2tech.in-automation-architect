package in.connect2tech.jpetstore.cucumber.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features/S_ABCD_02.feature", 
		glue = "in.connect2tech.jpetstore.cucumber.steps",dryRun = false)
public class DummyRunner {

}
