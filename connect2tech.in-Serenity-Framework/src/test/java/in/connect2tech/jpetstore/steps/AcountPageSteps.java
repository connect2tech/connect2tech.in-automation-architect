package in.connect2tech.jpetstore.steps;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class AcountPageSteps extends ScenarioSteps {

	AccountsPage accountPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * ACCOUNT PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/

	@Step("Adding new user information with userName: {0} ,password: {1} , repeatPassword: {2}")
	public AccountsPage addNewUserInformation(String userName, String password, String repeatPassword) {

		return accountPage.addNewUserInformation(userName, password, repeatPassword);
	}

	@Step("Adding account information- firstname:{0} , lastName:{1},"
			+ " email:{2}, phone:{3}, addr1:{4}, addr2: {5}, city:{6}, " + "state:{7}, zip:{8}, country:{9} ")
	public AccountsPage addAccountInformation(String firstName, String lastName, String email, String phone,
			String addr1, String addr2, String city, String state, String zip, String country) {

		return accountPage.addAccountInformation(firstName, lastName, email, phone, addr1, addr2, city, state, zip,
				country);
	}

	@Step("Adding profile information - language:{0} ,category: {1} ,myList: {2} ,myBanner:{3}")
	public AccountsPage addProfileInformation(String language, String category, boolean myList, boolean myBanner) {

		return accountPage.addProfileInformation(language, category, myList, myBanner);
	}

	@Step("Saving account information")
	public DashBoardPage clickSaveAccountInformation() {

		return accountPage.clickSaveAccountInformation();

	}

}
