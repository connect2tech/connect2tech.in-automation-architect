package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.BasePage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.HelpPage;
import in.connect2tech.jpetstore.pages.LoginPage;
import in.connect2tech.jpetstore.pages.OrdersPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class ProductsPageSteps extends ScenarioSteps {

	ProductsPage productsPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * PRODUCTS PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/

	@Step("Selecting a pet with petcategory:{0} , and petName:{1}")
	public ProductsPage selectPetByName(PetCategories petcategory, String petName) {

		return productsPage.selectPetByName(petcategory, petName);
	}

	@Step("Adding {0} to the shopping cart")
	public ProductsPage addToCartSpecificProduct(String specificProductName) {

		return productsPage.addToCartSpecificProduct(specificProductName);
	}

	@Step("Selecting pet {0} by viewing details and adding it to cart")
	public ProductsPage addToCartByViewingItemDetails(String specificProduct, String... details) {

		return productsPage.addToCartByViewingItemDetails(specificProduct, details);
	}

	@Step("Selecting {0} from the search results")
	public ProductsPage selectProductFromSearchTable(String productName) {

		return productsPage.selectProductFromSearchTable(productName);
	}

	@Step("Interacting with the shopping cart")
	public ProductsPage shoppingCart(String productDescription, int quantity) {

		return productsPage.shoppingCart(productDescription, quantity);
	}

	@Step("Remove item {0} from shopping cart")
	public boolean removeItemFromCart(String productName) {

		return productsPage.removeItemFromCart(productName);
	}

	@Step("Click on Proceed to checkout")
	public OrdersPage clickOnProceedToCheckout() {

		return productsPage.clickOnProceedToCheckout();
	}

}
