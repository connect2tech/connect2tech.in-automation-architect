package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.BasePage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.HelpPage;
import in.connect2tech.jpetstore.pages.LoginPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class BasePageSteps extends ScenarioSteps {

	BasePage basePage;

	/**
	 * This link is active when user logs in.
	 * 
	 * @return DashBoardPage
	 */
	@Step("Signing out from the store")
	public DashBoardPage signOut() {

		return basePage.signOut();
	}

	@Step("Navigating to Login Page")
	public LoginPage navigateToLoginPage() {

		return basePage.navigateToSignOnPage();
	}
	
	@Step("Navigating to Login Page2")
	public LoginPage navigateToLoginPage2() {

		return basePage.navigateToSignOnPage2();
	}

	@Step("Navigating to products page by cliking on : {0} link on header")
	public ProductsPage navigateToProductCategory(PetCategories productCategory) {

		return basePage.navigateToProductCategory(productCategory);
	}

	@Step("Navigating to Products Page by clicking on shopping cart")
	public ProductsPage navigateToShoppingCartPage() {

		return basePage.navigateToShoppingCart();
	}

	@Step("Navigating to Help Page")
	public HelpPage navigateToHelpPage() {

		return basePage.navigateToHelpPage();
	}

	@Step("Clicking on Company Logo & navigating to the DashBoard")
	public DashBoardPage navigateToDashBoard() {

		return basePage.navigateToDashBoard();
	}

	/**
	 * Navigating to Accounts Page. Link become active when user is logged in.
	 * 
	 * @return AccountsPage
	 */
	@Step("Navigating to Accounts Page")
	public AccountsPage navigateToMyAccountsPage() {

		return basePage.navigateToAccountPage();
	}

	@Step("Searching for product: {0}")
	public ProductsPage searchForProduct(String productName) {

		return basePage.searchForProduct(productName);
	}

}
