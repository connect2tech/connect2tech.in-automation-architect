package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.BasePage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.EntryPage;
import in.connect2tech.jpetstore.pages.HelpPage;
import in.connect2tech.jpetstore.pages.LoginPage;
import in.connect2tech.jpetstore.pages.OrdersPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class EntryPageSteps extends ScenarioSteps {

	EntryPage entryPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * ORDERS PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Click on <b>Enter the Store</b> to enter JPetStore")
	public DashBoardPage enterPetStore() {

		return entryPage.enterPetStore();

	}

	

}
