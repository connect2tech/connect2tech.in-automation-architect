package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.BasePage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.HelpPage;
import in.connect2tech.jpetstore.pages.LoginPage;
import in.connect2tech.jpetstore.pages.OrdersPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class OrderPageSteps extends ScenarioSteps {

	OrdersPage ordersPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * ORDERS PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/
	@Step("Entering purchase information & placing order")
	public OrdersPage enterPaymentAndBillingDetails(String cardType, String cardNumber, String expiryDate,
			String firstname, String lastname, String addr1, String addr2, String city, String state, String zip,
			String country) {

		return ordersPage.enterPaymentAndBillingDetails(cardType, cardNumber, expiryDate, firstname, lastname, addr1,
				addr2, city, state, zip, country);

	}

	@Step("Clicking on Ship to different checkbox")
	public OrdersPage clickShipToDifferentAddress() {

		return ordersPage.clickShipToDifferentAddress();
	}

	@Step("Entering Shipping information")
	public OrdersPage enterShippingInfo(String firstName, String lastName, String addr1, String addr2, String city,
			String state, String zip, String country) {

		return ordersPage.enterShippingInfo(firstName, lastName, addr1, addr2, city, state, zip, country);
	}

	@Step("Clicking on Continue Button")
	public OrdersPage clickOnContinueBtn() {

		return ordersPage.clickOnContinueBtn();
	}

	@Step("Clicking on Confirm button")
	public OrdersPage clickOnConfirmBtn() {

		return ordersPage.clickOnConfirmBtn();
	}

	@Step("Verify iof order has been placed")
	public void verifyIfOrderSubmitted() {

		ordersPage.verifyIfOrderSubmitted();
	}
}
