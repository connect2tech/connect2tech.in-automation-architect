package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class DashboardPageSteps extends ScenarioSteps {

	DashBoardPage dashBoardPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * DASHBOARD PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/

	@Step("Getting the greeting message")
	public String getGreetingMessage() {

		return dashBoardPage.getGreetingMessage();
	}

	@Step("Selecting {0} petcatgory form center display")
	public ProductsPage selectProductFromCenterDisplay(PetCategories petCategories) {

		return dashBoardPage.selectProductFromCenterDisplay(petCategories);
	}

	@Step("Selecting {0} petcatgory form side bar")
	public ProductsPage selectProductFromSideBar(PetCategories petCategories) {

		return dashBoardPage.selectProductFromSideBarDisplay(petCategories);
	}

}
