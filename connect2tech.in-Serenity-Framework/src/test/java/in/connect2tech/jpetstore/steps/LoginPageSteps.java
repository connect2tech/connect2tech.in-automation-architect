package in.connect2tech.jpetstore.steps;

import com.jpetstore.utils.PetCategories;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.BasePage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import in.connect2tech.jpetstore.pages.HelpPage;
import in.connect2tech.jpetstore.pages.LoginPage;
import in.connect2tech.jpetstore.pages.OrdersPage;
import in.connect2tech.jpetstore.pages.ProductsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginPageSteps extends ScenarioSteps {

	BasePage basePage;
	LoginPage loginPage;

	/************************************************************************************************
	 * **********************************************************************************************
	 * 
	 * LOGIN PAGE STEPS
	 * 
	 *************************************************************************************************
	 *************************************************************************************************/

	@Step("Getting Invalid Login Message")
	public String getMessageOnInvalidLogin() {

		return loginPage.getMessageOnInvalidLogin();
	}

	@Step("Navigating to user registration page")
	public AccountsPage navigateToRegistrationPage() {

		return loginPage.navigateToRegistrationPage();
	}

	@Step("Logging into the application with userName: {0} & passsword: {1}")
	public DashBoardPage doLogin(String userName, String password) {

		// To Do - Not sure about this one.
		basePage.clickSignOnLink();
		return loginPage.doLogin(userName, password);
	}

}
