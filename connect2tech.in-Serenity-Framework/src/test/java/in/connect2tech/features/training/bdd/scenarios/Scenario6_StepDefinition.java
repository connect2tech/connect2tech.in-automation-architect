package in.connect2tech.features.training.bdd.scenarios;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Scenario6_StepDefinition {
	@Given("^I try to create an account with password \"([^\"]*)\"$")
	public void i_try_to_create_an_account_with_password(String arg1) {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@Then("^I should see that the password is invalid$")
	public void i_should_see_that_the_password_is_invalid() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@Then("^I should see that the password is valid$")
	public void i_should_see_that_the_password_is_valid() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

}
