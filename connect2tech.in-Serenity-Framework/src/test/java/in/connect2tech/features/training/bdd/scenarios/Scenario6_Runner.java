package in.connect2tech.features.training.bdd.scenarios;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/training/bdd/scenarios/scenario6_multiple_tables.feature", 
glue = "in.connect2tech.features.training.bdd.scenarios")
public class Scenario6_Runner {

}
