package in.connect2tech.features.training.bdd.scenarios;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Scenario1_StepDefinition {
	@Given("^I have been issued a new card$")
	public void i_have_been_issued_a_new_card() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}


	@Given("^I insert the card, entering the correct PIN$")
	public void i_insert_the_card_entering_the_correct_PIN() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^I choose \"([^\"]*)\" from the menu$")
	public void i_choose_from_the_menu(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^I change the PIN to (\\d+)$")
	public void i_change_the_PIN_to(int arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^the system should remember my PIN is now (\\d+)$")
	public void the_system_should_remember_my_PIN_is_now(int arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^I try to change the PIN to the original PIN number$")
	public void i_try_to_change_the_PIN_to_the_original_PIN_number() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^I should see a warning message$")
	public void i_should_see_a_warning_message() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^the system should not have changed my PIN$")
	public void the_system_should_not_have_changed_my_PIN() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

}
