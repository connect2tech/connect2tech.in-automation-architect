package in.connect2tech.features.training.bdd.scenarios;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/training/bdd/scenarios/scenario4.feature", 
glue = "")
public class Scenario4_Runner {

}
