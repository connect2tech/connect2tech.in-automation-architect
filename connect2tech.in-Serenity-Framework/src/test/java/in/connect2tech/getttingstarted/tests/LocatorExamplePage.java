package in.connect2tech.getttingstarted.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

public class LocatorExamplePage extends PageObject {
	public LocatorExamplePage(WebDriver driver) {
		super();
	}

	public void selenium_clickOnLink() {
		
		open();
		
		WebElement we = getDriver().findElement(By.linkText("Enter the Store"));
		we.click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
