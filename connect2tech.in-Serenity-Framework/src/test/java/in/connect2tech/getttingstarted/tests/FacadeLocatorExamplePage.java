package in.connect2tech.getttingstarted.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class FacadeLocatorExamplePage extends PageObject {
	public FacadeLocatorExamplePage(WebDriver driver) {
		super();
	}

	public void selenium_clickOnLink() {

		open();
		//findBy(css or xpath) or $(css or xpath)
		//WebElementFacade wef = find(By.xpath("/html/body/div[1]/div/div[2]/nav/div/div[2]/ul[2]/a"));
		//WebElementFacade wef = find(By.cssSelector("css"),By.xpath("/html/body/div[1]/div/div[2]/nav/div/div[2]/ul[2]/a"));
		//WebElementFacade wef = findBy("/html/body/div[1]/div/div[2]/nav/div/div[2]/ul[2]/a");
		//WebElementFacade wef = $("/html/body/div[1]/div/div[2]/nav/div/div[2]/ul[2]/a");
		
		//CSS or XPath
		WebElementFacade wef = $("//*[@id='Content']/p[1]/a");
		
		
		wef.click();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
