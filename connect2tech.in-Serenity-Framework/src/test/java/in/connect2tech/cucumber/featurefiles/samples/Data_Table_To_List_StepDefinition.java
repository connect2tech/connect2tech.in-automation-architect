package in.connect2tech.cucumber.featurefiles.samples;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Data_Table_To_List_StepDefinition {
	@Given("^a list of numbers$")
	public void a_list_of_numbers(List<Integer> arg1) {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc).
		// Field names for YourType must match the column names in
		// your feature file (except for spaces and capitalization).
		//throw new PendingException();

		System.out.println("arg1-------------------------->"+arg1);
	
	}

	@When("^I summarize them$")
	public void i_summarize_them() {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@Then("^should I get (\\d+)$")
	public void should_I_get(int arg1) {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

}
