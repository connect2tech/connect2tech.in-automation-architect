package in.connect2tech.cucumber.featurefiles.samples;

public class Price {
	private String product;
	private Integer price;
	private String currency;

	public Price(String product, Integer price, String currency) {
		this.product = product;
		this.price = price;
		this.currency = currency;
	}

	public String getProduct() {
		return product;
	}

	public Integer getPrice() {
		return price;
	}

	public String getCurrency() {
		return currency;
	}

	public String toString() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("prodct=").append(getProduct()).append(", price=").append(getProduct()).append("currency=")
				.append(getCurrency());

		return sbuf.toString();
	}
}