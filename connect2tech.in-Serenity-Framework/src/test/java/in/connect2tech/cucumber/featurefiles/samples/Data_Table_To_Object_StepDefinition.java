package in.connect2tech.cucumber.featurefiles.samples;

import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Data_Table_To_Object_StepDefinition {

	@Given("^the price list for an international coffee shop$")
	public void the_price_list_for_an_international_coffee_shop(List<Price> p) {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc).
		// Field names for YourType must match the column names in
		// your feature file (except for spaces and capitalization).
		//throw new PendingException();
		
		System.out.println("p==============================>"+p);
	}

	@When("^I buy (\\d+) coffee$")
	public void i_buy_coffee(int arg1) {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@When("^I buy (\\d+) donut$")
	public void i_buy_donut(int arg1) {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

	@Then("^should I pay (\\d+) EUR and (\\d+) SEK$")
	public void should_I_pay_EUR_and_SEK(int arg1, int arg2) {
		// Write code here that turns the phrase above into concrete actions
		//throw new PendingException();
	}

}
