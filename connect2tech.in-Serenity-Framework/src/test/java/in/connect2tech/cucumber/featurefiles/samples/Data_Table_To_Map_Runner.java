package in.connect2tech.cucumber.featurefiles.samples;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/samples/Data_Table_To_Map.feature", 
glue = "in.connect2tech.cucumber.featurefiles.samples")
public class Data_Table_To_Map_Runner {

}
