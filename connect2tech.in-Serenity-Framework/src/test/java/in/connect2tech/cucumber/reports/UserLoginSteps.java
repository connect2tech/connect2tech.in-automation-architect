package in.connect2tech.cucumber.reports;

import in.connect2tech.jpetstore.pages.AccountsPage;
import in.connect2tech.jpetstore.pages.DashBoardPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class UserLoginSteps extends ScenarioSteps {

	@Step("Open URL: {0} ")
	public AccountsPage openURL(String url) {
		return null;
	}
	
	@Step("Login with userName: {0} ,password: {1} ")
	public AccountsPage userLogin(String userName, String password) {
		return null;
	}

	@Step("Click Login Button")
	public DashBoardPage clickLoginButton() {
		return null;
	}

}
