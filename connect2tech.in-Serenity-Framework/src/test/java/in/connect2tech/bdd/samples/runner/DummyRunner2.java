package in.connect2tech.bdd.samples.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features/samples/Sample2_Data_Table_To_List.feature", 
		glue="in.connect2tech.bdd.samples.steps")
public class DummyRunner2 {

}
