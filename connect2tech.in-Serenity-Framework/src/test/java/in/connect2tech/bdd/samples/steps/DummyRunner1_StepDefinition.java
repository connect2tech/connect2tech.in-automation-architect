package in.connect2tech.bdd.samples.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DummyRunner1_StepDefinition {
	@Given("A Branch {string} has authority to make order or return request")
	public void a_Branch_has_authority_to_make_order_or_return_request(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}

	@When("User login into {string}")
	public void user_login_into(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}

	@Then("User Navigates to {string}")
	public void user_Navigates_to(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}

	@Then("Validate The screen details {string} and {string} and {string} and {string}")
	public void validate_The_screen_details_and_and_and(String string, String string2, String string3, String string4) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}

	@Then("Validate details against database")
	public void validate_details_against_database() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new cucumber.api.PendingException();
	}

}
