connect2tech.in-Serenity-WebDriver-Framework
============================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.



## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French



1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


# Important Links
- [Bitbucket Repositories](https://bitbucket.org/connect2tech)
- [Serenity Documentation](http://thucydides.info/docs/serenity/#introduction)

# Versions

| serenity.version  | serenity.maven.version |serenity.cucumber.version |
| ------------- | ------------- |------------- |
| 2.0.69  | 2.0.69  |1.9.21  |

# Reporting

- serenity.test.root=in.connect2tech.cucumber
- serenity.requirements.types=theme,epic,story
- in.connect2tech.jpetstore.cucumber.runner.DummyRunner 
- Step Definition not defined

### Samlink ABCD Example
- in.connect2tech.jpetstore.cucumber.runner.UserRegistrationRunner.java
- /Samlink-Serenity-WebUI-BaseLine-FrameWork/src/test/resources/features/user-registration.feature

### Working Example1

- src\test\resources\features\S_ABCD_02.feature

```
Feature: Organization data Display 
Scenario Outline: ABCD 
	Given A Branch "<User>" has authority to make order or return request 
	When User login into "<Application_Name>" 
	Then User Navigates to "<Page>" 
	And Validate The screen details "<Ordering_Bank_Number>" and "<Branch_Number>" and "<Branch_User_ID>" and "<User_Name>" 
	And Validate details against database 
	Examples: Too Short 
		|Application_Name|User|Page|Ordering_Bank_Number|Branch_Number|Branch_User_ID|User_Name|
		|Rahahuolto Application|User1|Euro_Note_Order|BranchBankNo-001 | BranchNo-001 |BranchUserId-001 |UserName-001|
		|Rahahuolto Application|User2|Euro_Note_Order|BranchBankNo-002 | BranchNo-002 |BranchUserId-002 |UserName-002|
```

### Sample BDDs

###### Sample1_Examples
- src\test\resources\features\samples\Sample1_Examples.feature
- in.connect2tech.bdd.samples.runner.DummyRunner1.java
- in.connect2tech.bdd.samples.steps.DummyRunner1_StepDefinition.java

```
Feature: Organization data Display 

Scenario Outline: Sample1 

	Given A Branch "<User>" has authority to make order or return request 
	When User login into "<Application_Name>" 
	Then User Navigates to "<Page>" 
	And Validate The screen details "<Ordering_Bank_Number>" and "<Branch_Number>" and "<Branch_User_ID>" and "<User_Name>" 
	And Validate details against database 
	Examples: Too Short 
		|Application_Name|User|Page|Ordering_Bank_Number|Branch_Number|Branch_User_ID|User_Name|
		|Rahahuolto Application|User1|Euro_Note_Order|BranchBankNo-001 | BranchNo-001 |BranchUserId-001 |UserName-001|
		|Rahahuolto Application|User2|Euro_Note_Order|BranchBankNo-002 | BranchNo-002 |BranchUserId-002 |UserName-002|
```

###### Sample2_Data_Table_To_List
- src\test\resources\features\samples\Sample2_Data_Table_To_List.feature
- in.connect2tech.bdd.samples.runner.DummyRunner1.java

```
Feature: Data Table Feature File
Cucumber can convert a Gherkin data table to a list of a type you specify. 

Scenario: The sum of a list of numbers should be calculated 
	Given a list of numbers 
		| 17   |
		| 42   |
		| 4711 |
	When I summarize them 
	Then should I get 4770
```

###### Sample2_Given_Data_Then_Data
- src\test\resources\features\samples\Sample2_Given_Data_Then_Data.feature
- in.connect2tech.bdd.samples.runner.DummyRunner1.java

```
Feature: Using Data in Given and Then 

Scenario: Transferring funds between internal accounts 
	Given Clive has the following accounts: 
		| Account | Balance |
		| Current | 1000    |
		| Savings | 2000    |
	When he transfers $100 from his Current account to his Savings account 
	Then his new account balances should be: 
		| Account | Balance |
		| Current | 900     |
		| Savings | 2100    |
```


		
### Examples
- MyFirstTest.java - Simple program using @Test, @Managed WebDriver and open a URL.

- LocatorExampleTest.java - Simple program using @Test, @Managed WebDriver, autowiring to inject PageObject in test class.

- FacadeLocatorExampleTest.java - Simple program using @Test, autowiring to inject POM, PageObject, WebElementFacade (http://localhost:8080/jpetstore/)

- EditBoxTest.java - Simple program using @Test, autowiring to PageObject, WebElementFacade 

- DropDownTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to select drop down

- CheckBoxTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to handle check box

- RadioButtonTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to handle radio button

- TableTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to access html table (http://localhost:8888/connect2tech.in-Selenium-Automation-Java-1.x/)

- AlertsTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, handling different alerts (http://localhost:8888/connect2tech.in-Selenium-Automation-Java-1.x/)

- ImplicitWaitExamplePage.java - How to use implicit wait

- ExplicitWaitExamplePage.java - How to use explicit wait

- RunnerTest.java - Test Cases 
    - @Managed WebDriver driver - Driver is injected automatically, based on serenity.properties configuration
    - @Steps BasePageSteps basePageSteps - Injected
    - @Test @Title("Navigate to Sign On Page") - Test Case


### Test Case, Steps, POM Design (Test Case=>Steps=>POM)

### Examples: BDD
- ShoppingCartRunner.java - Simple program using @Given, @When, @Then, @Step and @Pending 
- UserRegistrationRunner.java - 

### Sample feature files
- Data_Table_To_List.feature (list without heading)


### maven-surefire-plugin

https://antoniogoncalves.org/2012/12/13/lets-turn-integration-tests-with-maven-to-a-first-class-citizen/

The Maven lifecycle has four phases for running integration tests:

- pre-integration-test: on this phase you can start any required service or do any action (starting a database, a webserver)
- integration-test: failsafe will run the test on this phase
- post-integration-test: time to shutdown all services
- verify: failsafe runs another goal that interprets the results of tests here

The Failsafe Plugin has only 2 goals:

- failsafe:integration-test : runs the integration tests of an application.
- failsafe:verify : verifies that the integration tests of an application passed.

http://thucydides.info/docs/serenity-staging/

Next, you need to add and configure the serenity-maven-plugin.Pt. 5 A useful technique is to bind the aggregate goal plugin to the post-integration-test phase. Pt.6 and Pt.7 This way, to run the tests and to generate the reports, you would run the following

mvn verify
