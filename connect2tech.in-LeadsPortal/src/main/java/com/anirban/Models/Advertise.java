package com.anirban.Models;

import java.util.HashMap;

public class Advertise {

	private String title;
	private String category;
	private String posted;
	private Customer customer;
	private String locationPreference;

	public static String[] AdvertiseParameters = { "Title", "Category", "Posted", "Customer Name", "Customer Location",
			"Customer Availability", "Location Preference" };
	public static int[] AdvertiseParamColumnLen={9500,6000,4000,4000,7000,5500,11500};

	public Advertise() {
	}

	public Advertise(String title, String category, String posted, String name, String location, String availability,
			String locationPreference) {
		super();
		this.title = title;
		this.category = category;
		this.posted = posted;
		this.customer = new Customer(name, location, availability);
		this.locationPreference = locationPreference;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}

	public String getPosted() {
		return posted;
	}

	public Customer getCustomer() {
		return customer;
	}

	public String getLocationPreference() {
		return locationPreference;
	}

	public HashMap<Integer, String> returnParamValues() {
		
		HashMap<Integer, String> map = new HashMap<>();
		int i = 0;
		map.put(i++, this.title);
		map.put(i++, this.category);
		map.put(i++, this.posted);
		map.put(i++, this.customer.getName());
		map.put(i++, this.customer.getLocation());
		map.put(i++, this.customer.getAvailability());
		map.put(i++, this.locationPreference);
		
		return map;

	}

	@Override
	public String toString() {
		return "Advertise [Title=" + title + ", Category=" + category + ", Posted=" + posted + ", " + customer
				+ ", Location Preference=" + locationPreference + "]";
	}

}
