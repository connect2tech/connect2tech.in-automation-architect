package com.anirban.Models;

import java.util.ArrayList;
import java.util.HashMap;

public class AdvertiseGroup {

	private HashMap<String, ArrayList<Advertise>> collection = new HashMap<>();

	public AdvertiseGroup(ArrayList<Advertise> data) {

		for (Advertise currentAdvertise : data) {
			String currentTitle = currentAdvertise.getTitle();

			if ((currentTitle.toLowerCase()).contains("java script")) {

				if (!collection.containsKey("Java Script")) {
					collection.put("Java Script", new ArrayList<Advertise>());
				}
				collection.get("Java Script").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("java")) {

				if (!collection.containsKey("Java")) {
					collection.put("Java", new ArrayList<Advertise>());
				}
				collection.get("Java").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("c++")) {

				if (!collection.containsKey("C++")) {
					collection.put("C++", new ArrayList<Advertise>());
				}
				collection.get("C++").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("c sharp")) {

				if (!collection.containsKey("C Sharp")) {
					collection.put("C Sharp", new ArrayList<Advertise>());
				}
				collection.get("C Sharp").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains(" c ") || (currentTitle.toLowerCase()).startsWith("c ")) {

				if (!collection.containsKey("C")) {
					collection.put("C", new ArrayList<Advertise>());
				}
				collection.get("C").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("python")) {

				if (!collection.containsKey("Python")) {
					collection.put("Python", new ArrayList<Advertise>());
				}
				collection.get("Python").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("microsoft excel")) {

				if (!collection.containsKey("Microsoft Excel")) {
					collection.put("Microsoft Excel", new ArrayList<Advertise>());
				}
				collection.get("Microsoft Excel").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("sql")) {

				if (!collection.containsKey("SQL")) {
					collection.put("SQL", new ArrayList<Advertise>());
				}
				collection.get("SQL").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("angular.js")) {

				if (!collection.containsKey("Angular.JS")) {
					collection.put("Angular.JS", new ArrayList<Advertise>());
				}
				collection.get("Angular.JS").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("oracle")) {

				if (!collection.containsKey("Oracle")) {
					collection.put("Oracle", new ArrayList<Advertise>());
				}
				collection.get("Oracle").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("selenium")) {

				if (!collection.containsKey("Selenium")) {
					collection.put("Selenium", new ArrayList<Advertise>());
				}
				collection.get("Selenium").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("informatica")) {

				if (!collection.containsKey("Informatica")) {
					collection.put("Informatica", new ArrayList<Advertise>());
				}
				collection.get("Informatica").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("big data")) {

				if (!collection.containsKey("Big Data")) {
					collection.put("Big Data", new ArrayList<Advertise>());
				}
				collection.get("Big Data").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("hadoop")) {

				if (!collection.containsKey("Hadoop")) {
					collection.put("Hadoop", new ArrayList<Advertise>());
				}
				collection.get("Hadoop").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("istqb")) {

				if (!collection.containsKey("ISTQB")) {
					collection.put("ISTQB", new ArrayList<Advertise>());
				}
				collection.get("ISTQB").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("node.js")) {

				if (!collection.containsKey("Node.JS")) {
					collection.put("Node.JS", new ArrayList<Advertise>());
				}
				collection.get("Node.JS").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("software testing")) {

				if (!collection.containsKey("Software Testing")) {
					collection.put("Software Testing", new ArrayList<Advertise>());
				}
				collection.get("Software Testing").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("tableau")) {

				if (!collection.containsKey("Tableau")) {
					collection.put("Tableau", new ArrayList<Advertise>());
				}
				collection.get("Tableau").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("data science")) {

				if (!collection.containsKey("Data Science")) {
					collection.put("Data Science", new ArrayList<Advertise>());
				}
				collection.get("Data Science").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("salesforce")) {

				if (!collection.containsKey("Salesforce")) {
					collection.put("Salesforce", new ArrayList<Advertise>());
				}
				collection.get("Salesforce").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("microsoft azure")) {

				if (!collection.containsKey("Microsoft Azure")) {
					collection.put("Microsoft Azure", new ArrayList<Advertise>());
				}
				collection.get("Microsoft Azure").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("php")) {

				if (!collection.containsKey("PHP")) {
					collection.put("PHP", new ArrayList<Advertise>());
				}
				collection.get("PHP").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("adobe photoshop")) {

				if (!collection.containsKey("Adobe Photoshop")) {
					collection.put("Adobe Photoshop", new ArrayList<Advertise>());
				}
				collection.get("Adobe Photoshop").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("sap")) {

				if (!collection.containsKey("SAP")) {
					collection.put("SAP", new ArrayList<Advertise>());
				}
				collection.get("SAP").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("devops")) {

				if (!collection.containsKey("DevOps")) {
					collection.put("DevOps", new ArrayList<Advertise>());
				}
				collection.get("DevOps").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("mobile app")) {

				if (!collection.containsKey("Mobile App")) {
					collection.put("Mobile App", new ArrayList<Advertise>());
				}
				collection.get("Mobile App").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("matlab")) {

				if (!collection.containsKey("MATLAB")) {
					collection.put("MATLAB", new ArrayList<Advertise>());
				}
				collection.get("MATLAB").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("tally software")) {

				if (!collection.containsKey("Tally Software")) {
					collection.put("Tally Software", new ArrayList<Advertise>());
				}
				collection.get("Tally Software").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("autocad")) {

				if (!collection.containsKey("Autocad")) {
					collection.put("Autocad", new ArrayList<Advertise>());
				}
				collection.get("Autocad").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("data analysis")) {

				if (!collection.containsKey("Data Analysis")) {
					collection.put("Data Analysis", new ArrayList<Advertise>());
				}
				collection.get("Data Analysis").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("machine learning")) {

				if (!collection.containsKey("Machine Learning")) {
					collection.put("Machine Learning", new ArrayList<Advertise>());
				}
				collection.get("Machine Learning").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains(".net")) {

				if (!collection.containsKey(".Net")) {
					collection.put(".Net", new ArrayList<Advertise>());
				}
				collection.get(".Net").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("abinitio")) {

				if (!collection.containsKey("Abinitio")) {
					collection.put("Abinitio", new ArrayList<Advertise>());
				}
				collection.get("Abinitio").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("web designing")) {

				if (!collection.containsKey("Web Designing")) {
					collection.put("Web Designing", new ArrayList<Advertise>());
				}
				collection.get("Web Designing").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("web development")) {

				if (!collection.containsKey("Web Development")) {
					collection.put("Web Development", new ArrayList<Advertise>());
				}
				collection.get("Web Development").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("embedded systems")) {

				if (!collection.containsKey("Embedded Systems")) {
					collection.put("Embedded Systems", new ArrayList<Advertise>());
				}
				collection.get("Embedded Systems").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("unix")) {

				if (!collection.containsKey("Unix")) {
					collection.put("Unix", new ArrayList<Advertise>());
				}
				collection.get("Unix").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("ethical hacking")) {

				if (!collection.containsKey("Ethical Hacking")) {
					collection.put("Ethical Hacking", new ArrayList<Advertise>());
				}
				collection.get("Ethical Hacking").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("animation")) {

				if (!collection.containsKey("Animation")) {
					collection.put("Animation", new ArrayList<Advertise>());
				}
				collection.get("Animation").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("mainframe")) {

				if (!collection.containsKey("Mainframe")) {
					collection.put("Mainframe", new ArrayList<Advertise>());
				}
				collection.get("Mainframe").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("cloud computing")) {

				if (!collection.containsKey("Cloud Computing")) {
					collection.put("Cloud Computing", new ArrayList<Advertise>());
				}
				collection.get("Cloud Computing").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("automation testing")) {

				if (!collection.containsKey("Automation Testing")) {
					collection.put("Automation Testing", new ArrayList<Advertise>());
				}
				collection.get("Automation Testing").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("ms office")) {

				if (!collection.containsKey("MS Office")) {
					collection.put("MS Office", new ArrayList<Advertise>());
				}
				collection.get("MS Office").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("graphic design")) {

				if (!collection.containsKey("Graphic Design")) {
					collection.put("Graphic Design", new ArrayList<Advertise>());
				}
				collection.get("Graphic Design").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("amazon web services")) {

				if (!collection.containsKey("Amazon Web Services")) {
					collection.put("Amazon Web Services", new ArrayList<Advertise>());
				}
				collection.get("Amazon Web Services").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("corel draw")) {

				if (!collection.containsKey("Corel DRAW")) {
					collection.put("Corel DRAW", new ArrayList<Advertise>());
				}
				collection.get("Corel DRAW").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("computer course")) {

				if (!collection.containsKey("Computer Course")) {
					collection.put("Computer Course", new ArrayList<Advertise>());
				}
				collection.get("Computer Course").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("etl")) {

				if (!collection.containsKey("ETL")) {
					collection.put("ETL", new ArrayList<Advertise>());
				}
				collection.get("ETL").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("testing")) {

				if (!collection.containsKey("Other Testing")) {
					collection.put("Other Testing", new ArrayList<Advertise>());
				}
				collection.get("Other Testing").add(currentAdvertise);

			} else if ((currentTitle.toLowerCase()).contains("msbi")) {

				if (!collection.containsKey("MSBI")) {
					collection.put("MSBI", new ArrayList<Advertise>());
				}
				collection.get("MSBI").add(currentAdvertise);
			} else {
				if (!collection.containsKey("Others")) {
					collection.put("Others", new ArrayList<Advertise>());
				}
				collection.get("Others").add(currentAdvertise);
			}
		}
	}

	public void display() {
		collection.forEach((key,value)->{
			System.out.println("{ "+key+" -->("+value+" )}");
		});
	}

	public HashMap<String, ArrayList<Advertise>> getCollection() {
		return collection;
	}

}
