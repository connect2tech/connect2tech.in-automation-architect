package com.anirban.Models;

public class Customer {

	private String name;
	private String location;
	private String availability;
	
	public Customer(){}
	
	public Customer(String name, String location, String availability) {
		this.name = name;
		this.location = location;
		this.availability = availability;
	}

	public String getName() {
		return name;
	}
	public String getLocation() {
		return location;
	}


	public String getAvailability() {
		return availability;
	}
	
	@Override
	public String toString() {
		return "Customer [Name=" + name + ", Location=" + location + ", Availability=" + availability + "]";
	}
	
}
