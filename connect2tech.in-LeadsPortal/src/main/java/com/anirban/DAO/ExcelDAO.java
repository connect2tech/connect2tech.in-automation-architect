package com.anirban.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.anirban.Exceptions.WrongIdentifierNameException;
import com.anirban.Exceptions.WrongParamException;
import com.anirban.Exceptions.WrongPropertiesNameException;
import com.anirban.Models.Advertise;
import com.anirban.Models.properties;

public class ExcelDAO {

	// *******************************************************************************************
	/*
	 * Update one row-column => cell value Author- Anirban Pal Date - 04.05.2018
	 */
	public static void updateCellValue(String path, String sheetName, String colName, int rowNum, String value)
			throws WrongParamException, WrongIdentifierNameException {

		try (FileInputStream fis = new FileInputStream(new File(path)); Workbook workBook = new XSSFWorkbook(fis)) {

			Sheet sheet = workBook.getSheet(sheetName);

			int colNum = getColNum(sheet, 0, colName);
			if (colNum >= 0) {
				Row objRow = sheet.getRow(rowNum);
				if (objRow == null) {
					objRow = sheet.createRow(rowNum);
				}

				Cell cell = null;
				cell = objRow.getCell(colNum);
				if (cell == null) {
					cell = objRow.createCell(colNum);
				}

				cell.setCellType(Cell.CELL_TYPE_STRING);
				cell.setCellValue(value);
				fis.close();
				FileOutputStream fout = new FileOutputStream(path);
				workBook.write(fout);
				fout.close();
				cell = null;
				objRow = null;
				sheet = null;
			} else {
				throw new WrongIdentifierNameException("Wrong column nam passed in function. Path : " + path
						+ " Sheet : " + sheetName + " colName : " + colName);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// ********************************************************************************************************

	// ********************************************************************************************************
	/*
	 * Colour one row-column => cell value Author- Anirban Pal Date - 04.05.2018
	 * 
	 * colourIndex==> IndexedColors.RED.getIndex()
	 */
	public static void colourCellValue(String path, String sheetName, String colName, int rowNum, short colourIndex) {

		try (FileInputStream fis = new FileInputStream(new File(path)); Workbook workBook = new XSSFWorkbook(fis)) {

			Sheet sheet = workBook.getSheet(sheetName);

			int colNum = getColNum(sheet, 0, colName);
			if (colNum >= 0) {
				Row objRow = sheet.getRow(rowNum);
				if (objRow == null) {
					objRow = sheet.createRow(rowNum);
				}

				Cell cell = null;
				cell = objRow.getCell(colNum);
				if (cell == null) {
					cell = objRow.createCell(colNum);
				}

				CellStyle objCellStyle = workBook.createCellStyle();
				objCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				objCellStyle.setFillBackgroundColor(colourIndex);
				cell.setCellStyle(objCellStyle);
				fis.close();
				FileOutputStream fout = new FileOutputStream(path);
				workBook.write(fout);
				fout.close();
				cell = null;
				objRow = null;
				sheet = null;
			} else {
				System.out.println("Wrong column nam passed in function. Path : " + path + " Sheet : " + sheetName
						+ " colName : " + colName);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// ********************************************************************************************

	// ********************************************************************************************
	// Return conlumn number for a String on a particular row
	// Author : Anirban Pal
	// Date : 28.04.2018
	public static int getColNum(Sheet objSheet, int rowNum, String keyWord) {

		int colNum = -1;
		Row tempRow = objSheet.getRow(rowNum);
		Iterator<Cell> cellIterator = tempRow.iterator();
		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			if (keyWord.equalsIgnoreCase(cell.getStringCellValue())) {
				colNum = cell.getColumnIndex();
			}
		}
		return colNum;
	}

	// *********************************************************************************************

	// **********************************************************************************************
	// Return last Row number for a Sheet
	// Author : Anirban Pal
	// Date : 28.04.2018
	public static int getLastRowNum(String objPath, String sheetName) {

		int lastRowNum = -1;
		try (FileInputStream fis = new FileInputStream(new File(objPath));
				Workbook objWorkBook = new XSSFWorkbook(fis)) {

			Sheet objSheet = objWorkBook.getSheet(sheetName);
			lastRowNum = objSheet.getLastRowNum();

			objSheet = null;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return lastRowNum;
	}

	// ***********************************************************************************************

	// ************************************************************************************************
	// Return all values from a mentioned column (1st Row as Parameter name)
	// Author : Anirban Pal
	// Date : 03.05.2018

	public static LinkedList<String> getColumnValues(String objPath, String sheetName, String paramName)
			throws WrongIdentifierNameException {

		try (FileInputStream fis = new FileInputStream(new File(objPath));
				Workbook objWorkBook = new XSSFWorkbook(fis)) {

			Sheet objSheet = objWorkBook.getSheet(sheetName);
			int colNum = getColNum(objSheet, 0, paramName);
			if (colNum >= 0) {
				LinkedList<String> list = new LinkedList<>();
				Iterator<Row> rowItr = objSheet.iterator();
				while (rowItr.hasNext()) {
					Row nextRow = rowItr.next();
					Cell objCell = nextRow.getCell(colNum);
					if (objCell != null) {
						System.out.println(objCell.getStringCellValue());
						list.add(objCell.getStringCellValue());
					}
				}
				return list;
			} else {
				throw new WrongIdentifierNameException("Incorrect identifier passed in function. File :" + objPath
						+ " Sheet : " + sheetName + " Identifier : " + paramName);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	// ***********************************************************************************************

	// ************************************************************************************************

	public static LinkedHashMap<String, String> getAttributeFromSheet(String objPath, String sheetName,
			String attribute, String scenario) throws FileNotFoundException {

		try (FileInputStream fis = new FileInputStream(new File(objPath));
				Workbook objWorkBook = new XSSFWorkbook(fis);) {

			Sheet objSheet = objWorkBook.getSheet(sheetName);

			int colScenario = getColNum(objSheet, 0, scenario);
			int colAttribute = getColNum(objSheet, 0, attribute);

			LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();

			Iterator<Row> rowIterator = objSheet.iterator();
			if (rowIterator.hasNext()) {
				rowIterator.next();
				// return
			} else {
				return null;
			}

			while (rowIterator.hasNext()) {

				Row nextRow = rowIterator.next();
				Cell objCell = nextRow.getCell(colScenario);
				String value = null;
				String key = null;
				if (objCell != null) {

					switch (objCell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						Double value1 = new Double(objCell.getNumericCellValue());
						value = String.valueOf(value1.intValue());
						break;
					case Cell.CELL_TYPE_STRING:
						value = objCell.getStringCellValue();

					}

				} else {
					continue;
				}

				objCell = null;

				objCell = nextRow.getCell(colAttribute);
				if (objCell != null) {
					key = objCell.getStringCellValue();
				} else {
					continue;
				}

				map.put(key, value);

			}

			objSheet = null;
			return map;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// ********************************************************************************************************

	// ********************************************************************************************************
	// Return a value for identifier from key-value pair from excel sheet
	// Author : Anirban Pal
	// Date : 03.05.2018
	public static String getIdentifierValueFromSheet(String objPath, String sheetName, String keyName, String valueName,
			String identifierName) {

		String identifierValue = null;

		try (FileInputStream fis = new FileInputStream(new File(objPath));
				Workbook objWorkBook = new XSSFWorkbook(fis)) {

			Sheet objSheet = objWorkBook.getSheet(sheetName);

			int colKey = getColNum(objSheet, 0, keyName);
			int colValue = getColNum(objSheet, 0, valueName);
			if (colKey < 0 || colValue < 0) {
				return null;
			}
			String keyValue = null;

			Iterator<Row> rowIterator = objSheet.iterator();
			if (rowIterator.hasNext()) {
				rowIterator.next();
			}

			while (rowIterator.hasNext()) {

				Row nextRow = rowIterator.next();
				Cell objCell = nextRow.getCell(colKey);
				switch (objCell.getCellType()) {
				case Cell.CELL_TYPE_NUMERIC:

					Double value = new Double(objCell.getNumericCellValue());
					keyValue = String.valueOf(value.intValue());
					break;
				case Cell.CELL_TYPE_STRING:
					keyValue = objCell.getStringCellValue();
				}

				objCell = null;

				if (keyValue.equals(identifierName)) {
					objCell = nextRow.getCell(colValue);
					switch (objCell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:

						Double value = new Double(objCell.getNumericCellValue());
						identifierValue = String.valueOf(value.intValue());
						break;
					case Cell.CELL_TYPE_STRING:
						identifierValue = objCell.getStringCellValue();
					}
				}

				if (identifierValue != null) {
					break;
				}
			}

			objSheet = null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return identifierValue;

	}

	// *************************************************************************************************

	// *************************************************************************************************
	// Adding key-value pair on properties object from excel sheet
	// Author : Anirban Pal
	// Date : 27.04.2018
	public static void addVariableFromSheetToProperties(String objPath, String sheetName, String keyName,
			String valueName, String propertyName) throws WrongPropertiesNameException, IOException {

		try (FileInputStream fis = new FileInputStream(new File(objPath));
				Workbook objWorkBook = new XSSFWorkbook(fis)) {
			Sheet objSheet = objWorkBook.getSheet(sheetName);

			int colKey = getColNum(objSheet, 0, keyName);
			int colValue = getColNum(objSheet, 0, valueName);

			Iterator<Row> rowIterator = objSheet.iterator();
			if (rowIterator.hasNext()) {
				rowIterator.next();
			}

			while (rowIterator.hasNext()) {

				Row nextRow = rowIterator.next();
				Cell objCell = nextRow.getCell(colKey);
				String key = objCell.getStringCellValue();
				objCell = null;

				String value = null;
				objCell = nextRow.getCell(colValue);
				switch (objCell.getCellType()) {
				case Cell.CELL_TYPE_NUMERIC:

					Double value1 = new Double(objCell.getNumericCellValue());
					value = String.valueOf(value1.intValue());
					break;
				case Cell.CELL_TYPE_STRING:
					value = objCell.getStringCellValue();

				}
				objCell = null;

				switch (propertyName) {
				case "global":
					properties.global.put(key, value);
					break;
				// case "instance":
				// properties.instance.put(key, value);
				// break;
				default:
					throw new WrongPropertiesNameException(
							"Wrong name passed to add in property. Propery name : " + propertyName);
				}
			}
			objSheet = null;
		}

	}

	// ****************************************************************************************************


	// ********************************************************************************************************

	public static void createExcel(String path, String sheetName, ArrayList<String> data) throws IOException {

		try (FileOutputStream fos = new FileOutputStream(new File(path)); Workbook objWorkBook = new XSSFWorkbook()) {

			Sheet objSheet = objWorkBook.createSheet(sheetName);
			for (int i = 0; i < data.size(); i++) {
				Row currentRow = objSheet.createRow(i);
				Cell currentCell = currentRow.createCell(0);
				currentCell.setCellValue(data.get(i));
			}

			objWorkBook.write(fos);

		}
	}

	//Create result file logic for SeleniumProject_DocumentationAutomation project
	public static void createOrUpdateExcel(String path, String sheetName, ArrayList<String> data) throws IOException {

		File file = new File(path);
		if (file.exists()) {
			try (FileInputStream fis = new FileInputStream(file); Workbook objWorkBook = new XSSFWorkbook(fis)) {

				Sheet objSheet = objWorkBook.createSheet(sheetName);
				for (int i = 0; i < data.size(); i++) {
					Row currentRow = objSheet.createRow(i);
					Cell currentCell = currentRow.createCell(0);
					currentCell.setCellValue(data.get(i));
				}

				fis.close();
				try (FileOutputStream fos = new FileOutputStream(file)) {
					objWorkBook.write(fos);
				}
			}
		} else {
			try (FileOutputStream fos = new FileOutputStream(new File(path));
					Workbook objWorkBook = new XSSFWorkbook()) {

				Sheet objSheet = objWorkBook.createSheet(sheetName);
				for (int i = 0; i < data.size(); i++) {
					Row currentRow = objSheet.createRow(i);
					Cell currentCell = currentRow.createCell(0);
					currentCell.setCellValue(data.get(i));
				}

				objWorkBook.write(fos);

			}
		}
	}
	
	
	//Create result file logic for UrbanProAutomation project
		public static void createOrUpdateExcelUPA(String path, String sheetName, ArrayList<Advertise> data) throws IOException {

			File file = new File(path);
			
			if (file.exists()) {
				try (FileInputStream fis = new FileInputStream(file); Workbook objWorkBook = new XSSFWorkbook(fis)) {
					
					CellStyle style = objWorkBook.createCellStyle();
					style.setWrapText(true);
					
					Sheet objSheet = objWorkBook.createSheet(sheetName);
					
					int length=Advertise.AdvertiseParameters.length;
					for(int i=0;i<length;i++){
						objSheet.setColumnWidth(i, Advertise.AdvertiseParamColumnLen[i]);
					}
					
					Row firstRow = objSheet.createRow(0);
					
					int i=0;
					for(String param:Advertise.AdvertiseParameters){
						Cell currentCell=firstRow.createCell(i++);
						currentCell.setCellValue(param);
						currentCell.setCellStyle(style);
					}
					
					for (i = 1; i <= data.size(); i++) {
						Row currentRow = objSheet.createRow(i);
						HashMap<Integer, String> map=data.get(i-1).returnParamValues();
						for(int j=0;j<length;j++){
							Cell currentCell = currentRow.createCell(j);
							currentCell.setCellValue(map.get(j));
							currentCell.setCellStyle(style);
						}

					}

					fis.close();
					try (FileOutputStream fos = new FileOutputStream(file)) {
						objWorkBook.write(fos);
					}
				}
			} else {
				try (FileOutputStream fos = new FileOutputStream(new File(path));
						Workbook objWorkBook = new XSSFWorkbook()) {

					CellStyle style = objWorkBook.createCellStyle();
					style.setWrapText(true);
					
					Sheet objSheet = objWorkBook.createSheet(sheetName);
					int length=Advertise.AdvertiseParameters.length;
					for(int i=0;i<length;i++){
						objSheet.setColumnWidth(i, Advertise.AdvertiseParamColumnLen[i]);
					}
					Row firstRow = objSheet.createRow(0);
					
					int i=0;
					for(String param:Advertise.AdvertiseParameters){
						Cell currentCell=firstRow.createCell(i++);
						currentCell.setCellValue(param);
						currentCell.setCellStyle(style);
					}
					
					for (i = 1; i <= data.size(); i++) {
						Row currentRow = objSheet.createRow(i);
						HashMap<Integer, String> map=data.get(i-1).returnParamValues();
						for(int j=0;j<length;j++){
							Cell currentCell = currentRow.createCell(j);
							currentCell.setCellValue(map.get(j));
							currentCell.setCellStyle(style);
						}

					}

					objWorkBook.write(fos);

				}
			}
		}
}
