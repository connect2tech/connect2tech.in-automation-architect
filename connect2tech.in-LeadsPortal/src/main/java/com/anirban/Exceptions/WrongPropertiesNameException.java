package com.anirban.Exceptions;

public class WrongPropertiesNameException extends Exception{
	
	public WrongPropertiesNameException(String errorMessage) {
		super(errorMessage);
	}

}
