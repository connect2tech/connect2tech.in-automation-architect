package com.anirban.Exceptions;

public class WrongParamException extends Exception{
	public WrongParamException(String errorMessage) {
		super(errorMessage);
	}
}
