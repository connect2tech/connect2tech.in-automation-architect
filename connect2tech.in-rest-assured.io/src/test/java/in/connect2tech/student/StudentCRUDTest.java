package in.connect2tech.student;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class StudentCRUDTest extends TestBase {

	List<String> courses = new ArrayList<String>();
	String email = "email6@gmail.com";
	String firstName = "firstName1";
	String lastName = "lastName1";
	String program = "program1";

	@Test(priority=1)
	public void test001_post() {

		courses.add("course1");

		Student s = new Student();
		s.setEmail(email);
		s.setFirstName(firstName);
		s.setLastName(lastName);
		s.setProgramme(program);
		s.setCourses(courses);

		Response response = RestAssured.
		given().contentType(ContentType.JSON).
		when().body(s).post();
		
		System.out.println("response.path(\"id\")======>"+response.asString());

	}
	
	
	@Test(priority=2)
	public void test001_getAll() {


		Response response = RestAssured.
		given().get("/list");
		
		
		System.out.println("response.asString()======>"+response.asString());
		
		List <Integer> list = response.path("id");
		System.out.println("list==>"+list);
		
		int size = list.size();
		Integer id = list.get(size-1);
		System.out.println("id=====>"+id);
		

	}

}
