package in.connect2tech.google.distance.matrix.api;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class RequestSpecificationExample {

	RequestSpecBuilder requestBuilder;
	static RequestSpecification requestSpec;

	/***
	 * Given I have this information When I perform this action Then this should
	 * be the output
	 */
	@BeforeClass
	public void setup() {

		RestAssured.baseURI = "https://maps.googleapis.com";
		RestAssured.basePath = "/maps/api";

		requestBuilder = new RequestSpecBuilder();
		requestBuilder.setBaseUri("https://maps.googleapis.com");
		requestBuilder.setBasePath("/maps/api");
		requestBuilder.
				addQueryParam("units", "imperial").
				addQueryParam("origins", "Washington,DC").
				addQueryParam("destinations", "New+York+City,NY").
				addQueryParam("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8");
		requestSpec = requestBuilder.build();
	
	}

	@Test
	public void getResponseBody() {

		long timeTaken = given().spec(requestSpec)
				.when().get("/distancematrix/json").time();
		System.out.println(timeTaken);

	}
}