package in.connect2tech.google.distance.framework;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Path {
	public static final String BASE_URI = "https://maps.googleapis.com";
	//BASE_PATH - It might differe based on classes
	public static final String BASE_PATH = "/maps/api";
	//public static final String STATUSES = "/1.1/statuses";
}
