package in.connect2tech.google.distance.matrix.api.get;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class GetRequestXml1 {

	
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://maps.googleapis.com";
		RestAssured.basePath = "/maps/api";
	}

	// https://maps.googleapis.com/maps/api/distancematrix/xml?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8

	
	@Test(priority=1)
	public void displayValidatableResponseBody() {
		
		ValidatableResponse validatableResponse =
				given().
					param("units", "imperial").
					param("origins", "Washington,DC").
					param("destinations", "New+York+City,NY").
					param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8").
				when().
					get("/distancematrix/xml").
				then().log().all();
		
	}
	
	@Test(priority=2)
	public void displayResponseBody() {
		
		Response response =
				given().
					param("units", "imperial").
					param("origins", "Washington,DC").
					param("destinations", "New+York+City,NY").
					param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8").
				when().
					get("/distancematrix/xml").
				then().log().all().extract().response();
		
		String responseString = response.asString();
		System.out.println("responseString==>"+responseString);
		
		String value = response.path("distancematrixresponse.row.element.duration.value");
		System.out.println("distancematrixresponse.row.element.duration.value==>" + value);

		XmlPath xmlPath = new XmlPath(responseString);
		String text = xmlPath.get("distancematrixresponse.row.element.duration.text");
		System.out.println("distancematrixresponse.row.element.duration.text==>" + text);
	}
	
}