package in.connect2tech.google.distance.matrix.api.get;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class GetCallsResponseValidation2 {
	/***
	 * Given I have this information When I perform this action Then this should
	 * be the output
	 */
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://maps.googleapis.com";
		RestAssured.basePath = "/maps/api";
	}

	@Test
	public void validateResponseStatusValueContent1() {
				given().
					param("units", "imperial").
					param("origins", "Washington,DC").
					param("destinations", "New+York+City,NY").
					param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8").
				when().
					get("/distancematrix/json").
				then().
					body("rows[0].elements[0].distance.text",equalTo("226 mi"));
	}
	
	@Test
	public void validateResponseStatusValueContent2() {
				given().
					param("units", "imperial").
					param("origins", "Washington,DC").
					param("destinations", "New+York+City,NY").
					param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8").
				when().
					get("/distancematrix/json").
				then().statusCode(200).
					log().all().
					and().
					body("rows[0].elements[0].distance.text",hasItem("226 mi")).
					and().
					contentType(ContentType.JSON);
	}
	
	@Test
	public void validateResponseStatusValueContent3() {
			Response response = 	given().
					param("units", "imperial").
					param("origins", "Washington,DC").
					param("destinations", "New+York+City,NY").
					param("key", "AIzaSyDt7LBT0-ksKzF4mGg29RLviTSKS_ndBG8").
				when().
					get("/distancematrix/json");
					
			List destination_addresses = response.path("destination_addresses");
			System.out.println(destination_addresses);
			
			
			List rows = response.path("rows");
			System.out.println(rows);
			
			Map map = (Map)rows.get(0);
			System.out.println(map);
			
			System.out.println(map.get("elements"));
			
	
	
	}
	
	
	
}