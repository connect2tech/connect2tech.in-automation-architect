package in.connect2tech.reqres.apis.get;

import static io.restassured.RestAssured.given;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class GetCallsKeyValueParamsDisplayResponse1 {
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}

	
	@Test
	public void testGetPassKeyValueParamsDisplayResponse() {
		Response response = 
				given().
					param("page", 2).
				when().
					get("/users");
		
		//Formatted data
		System.out.println("response.prettyPrint()=====>"+response.prettyPrint());
		System.out.println("response.body().asString()=====>"+response.body().asString());
	
	}
}