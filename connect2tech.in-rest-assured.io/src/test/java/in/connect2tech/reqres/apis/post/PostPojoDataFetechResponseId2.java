package in.connect2tech.reqres.apis.post;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import javafx.beans.binding.When;

import static org.hamcrest.Matchers.equalTo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PostPojoDataFetechResponseId2 {
	@BeforeClass
	public void setup() {
		RestAssured.baseURI = "https://reqres.in/";
		RestAssured.basePath = "/api";
	}

	@Test
	public void postDataGetIdResponse() {

		User user = new User();
		user.setJob("Automation Architect");
		user.setName("Cognizant");

		Response response = 
				given().
					body(user).
				when().
					post("/users");

		System.out.println(response.body().prettyPrint());

		String id1 = response.path("id");
		System.out.println("id1==>"+id1);
		
		

	}
	
	@Test
	public void postDataGetIdThenResponse() {

		User user = new User();
		user.setJob("Automation Architect");
		user.setName("Cognizant");

		Response response = 
				given().
					body(user).
				when().
					post("/users").
				then().extract().response();

		System.out.println(response.body().prettyPrint());

		String id2 = response.path("id");
		System.out.println("id2==>"+id2);
		
		JsonPath jsonPath = new JsonPath(response.asString());
		System.out.println("jsonPath.get(\"id\")==>"+jsonPath.get("id"));
		
		

	}
}