{
    "destination_addresses": [
        "New York, NY, USA"
    ],
    "origin_addresses": [
        "Washington, DC, USA"
    ],
    "rows": [
        {
            "elements": [
                {
                    "distance": {
                        "text": "226 mi",
                        "value": 363999
                    },
                    "duration": {
                        "text": "3 hours 51 mins",
                        "value": 13885
                    },
                    "status": "OK"
                }
            ]
        }
    ],
    "status": "OK"
}

========================================================================

{
    "id": "888",
    "createdAt": "2019-06-17T11:12:03.982Z"
}

========================================================================

rest.jar

http://localhost:8080/student/list
http://localhost:8080/student/1
