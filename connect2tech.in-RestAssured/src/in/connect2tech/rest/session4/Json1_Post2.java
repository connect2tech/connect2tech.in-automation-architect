package in.connect2tech.rest.session4;

import static org.testng.Assert.assertEquals;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.jsontype.impl.AsExistingPropertyTypeSerializer;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Json1_Post2 {

	@Test
	public void RegistrationSuccessful() {
		RestAssured.baseURI = "http://restapi.demoqa.com/customer";
		RequestSpecification request = RestAssured.given();

		JSONObject requestParams = new JSONObject();
		requestParams.put("FirstName", "Virender10"); // Cast
		requestParams.put("LastName", "Singh10");
		requestParams.put("UserName", "sdimpleuser11112dd20110");
		requestParams.put("Password", "password1");

		requestParams.put("Email", "111sfhk0@gmail.com");

		request.body(requestParams.toJSONString());

		Response response = request.post("/register");
		
		String body = response.getBody().asString();
		System.out.println("body="+body);

		/*ResponseBody responseBody = response.getBody();
		JsonToObjectFail jsonToObjectFail = responseBody.as(JsonToObjectFail.class);
		
		String faultId = jsonToObjectFail.getFaultId();
		String fault = jsonToObjectFail.getFault();
		
		assertEquals("FAULT_INVALID_POST_REQUEST", fault);
		assertEquals("Invalid post data, please correct the request", faultId);*/

	}
}
