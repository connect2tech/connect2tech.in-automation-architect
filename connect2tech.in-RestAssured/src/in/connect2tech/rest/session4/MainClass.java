package in.connect2tech.rest.session4;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MainClass {
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.age = 10;
		s1.name = "API";
	}
}
