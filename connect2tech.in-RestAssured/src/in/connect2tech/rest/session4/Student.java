package in.connect2tech.rest.session4;

import org.codehaus.groovy.classgen.asm.indy.IndyBinHelper;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Student {
	String name;
	int age;
}
