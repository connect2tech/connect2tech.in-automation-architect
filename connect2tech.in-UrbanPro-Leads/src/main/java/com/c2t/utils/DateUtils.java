package com.c2t.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DateUtils {
	public static void main(String[] args) {

		System.out.println(getDateDdMmYyyyHhMmSs());

	}

	public static String getDateDdMmYyyyHhMmSs() {
		LocalDateTime now = LocalDateTime.now();
		int year = now.getYear();
		int month = now.getMonthValue();
		int day = now.getDayOfMonth();
		int hour = now.getHour();
		int minute = now.getMinute();
		int second = now.getSecond();

		StringBuffer sbuf = new StringBuffer();
		sbuf.append(day).append(".").append(month).append(".").append(year).append("-").append(hour).append(".")
				.append(minute).append(".").append(second);
		
		return sbuf.toString();
	}

	public static String getDateDdMmYyyy() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		String strDate = formatter.format(date);
		return strDate;
	}

}
