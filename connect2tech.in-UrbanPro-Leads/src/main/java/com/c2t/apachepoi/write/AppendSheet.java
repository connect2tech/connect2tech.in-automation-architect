package com.c2t.apachepoi.write;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.io.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class AppendSheet {
	public static void main(String[] args) {
		write();
	}

	public static void write() {
		try {
			FileInputStream myxls = new FileInputStream("poi-testt.xlsx");
			XSSFWorkbook studentsSheet = new XSSFWorkbook(myxls);
			XSSFSheet worksheet = studentsSheet.getSheetAt(0);
			int a = worksheet.getLastRowNum();
			System.out.println(a);
			Row row = worksheet.createRow(++a);
			row.createCell(1).setCellValue("Dr.Hola");
			myxls.close();
			FileOutputStream output_file = new FileOutputStream(new File("poi-testt.xlsx"));
			// write changes
			studentsSheet.write(output_file);
			output_file.close();
			System.out.println(" is successfully written");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		System.out.println("done...");
	}
}
