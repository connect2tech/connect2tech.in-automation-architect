package com.c2t.apachepoi.write;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * A very simple program that writes some data to an Excel file using the Apache
 * POI library.
 * 
 * @author www.codejava.net
 *
 */
public class WriteToExcelCreateWorkbook {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();

		String excelFilePath = "NiceJavaBooks.xlsx";
		
		for (int i = 0; i < 5; i++) {
			//XSSFSheet sheet = workbook.createSheet("Java Books"+i);

			List<Book> listBook = getListBook();
			

			//writeExcel(listBook, excelFilePath, workbook, i);
			int rowCount = 0;
			XSSFSheet sheet = workbook.createSheet("Java Books"+i);
			for (Book aBook : listBook) {
				Row row = sheet.createRow(++rowCount);
				writeBook(aBook, row);
			}
		}
		
		try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
			workbook.write(outputStream);
			outputStream.flush();
		}
		
		System.out.println("Done...");
	}

	private static void writeBook(Book aBook, Row row) {
		Cell cell = row.createCell(1);
		cell.setCellValue(aBook.getTitle());

		cell = row.createCell(2);
		cell.setCellValue(aBook.getAuthor());

		cell = row.createCell(3);
		cell.setCellValue(aBook.getPrice());
	}

	private static List<Book> getListBook() {
		Book book1 = new Book("Head First Java", "Kathy Serria", 79);
		Book book2 = new Book("Effective Java", "Joshua Bloch", 36);
		Book book3 = new Book("Clean Code", "Robert Martin", 42);
		Book book4 = new Book("Thinking in Java", "Bruce Eckel", 35);

		List<Book> listBook = Arrays.asList(book1, book2, book3, book4);

		return listBook;
	}

}