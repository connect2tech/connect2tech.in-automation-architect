package com.c2t.apachepoi.write;

import java.util.List;

public class UrbanProBeanITCoursesPune {
	String courseName;
	String customerName;
	String location;
	boolean isPhoneAvailable;
	List<String> locationPreference;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isPhoneAvailable() {
		return isPhoneAvailable;
	}

	public void setPhoneAvailable(boolean isPhoneAvailable) {
		this.isPhoneAvailable = isPhoneAvailable;
	}

	public List<String> getLocationPreference() {
		return locationPreference;
	}

	public void setLocationPreference(List<String> locationPreference) {
		this.locationPreference = locationPreference;
	}

}
