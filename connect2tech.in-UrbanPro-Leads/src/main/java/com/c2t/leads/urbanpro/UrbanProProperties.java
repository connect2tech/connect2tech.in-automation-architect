package com.c2t.leads.urbanpro;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class UrbanProProperties {

	public static Properties getProperties() {

		Properties prop = new Properties();

		try {

			InputStream input = new FileInputStream("src/main/resources/com/c2t/leads/urbanpro/urbanpro.properties");
			prop.load(input);
			// System.out.println(prop);
			Set keys = prop.keySet();
			Iterator iter = keys.iterator();

			while (iter.hasNext()) {
				String key = (String) iter.next();
				String value = prop.getProperty(key);
				//System.out.println("key=" + key +",	value=" + value);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {

		}

		return prop;
	}

	public static void main(String[] args) {
		getProperties();
	}

}
