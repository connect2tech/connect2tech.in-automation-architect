package com.selenium.ff;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LocatingElements15_10_2016 {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumUrbanPro/src/main/resources/com/selenium/ff/Selenium15_10_2016.html";
		driver.get(baseUrl);
		
		// finds a link element by the exact text it displays
		List <WebElement> l =  driver.findElements(By.className("w3-input1"));
		
		for(WebElement we : l){
			String s = we.findElement(By.className("w3-input2")).getText();
			System.out.println(s);
		}
				
		driver.close();
		
	}

}
