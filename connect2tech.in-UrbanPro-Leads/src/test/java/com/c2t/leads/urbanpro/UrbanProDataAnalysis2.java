package com.c2t.leads.urbanpro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.apachepoi.write.Book;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;

public class UrbanProDataAnalysis2 {

	WebDriver driver = null;
	List<WebElement> aStep = null;
	WebElement paginate = null;
	WebDriverWait wait = null;
	int iPunePages;

	List<String> java = new ArrayList<String>();
	List<String> selenium = new ArrayList<String>();
	List<String> excel = new ArrayList<String>();
	List<String> cLanguage = new ArrayList<String>();
	List<String> cpp = new ArrayList<String>();
	List<String> xml = new ArrayList<String>();
	List<String> linux = new ArrayList<String>();
	List<String> sql = new ArrayList<String>();
	List<String> dotNet = new ArrayList<String>();
	List<String> hadoop = new ArrayList<String>();
	List<String> spring = new ArrayList<String>();
	List<String> hibernate = new ArrayList<String>();
	List<String> unix = new ArrayList<String>();
	List<String> softwareTesting = new ArrayList<String>();
	List<String> plSQL = new ArrayList<String>();
	List<String> bigData = new ArrayList<String>();
	List<String> angularJs = new ArrayList<String>();
	List<String> mobile = new ArrayList<String>();
	List<String> automation = new ArrayList<String>();
	List<String> javaScript = new ArrayList<String>();
	List<String> webDevelopment = new ArrayList<String>();
	List<String> others = new ArrayList<String>();
	static String url;
	static Properties urbanPro;

	final static Logger logger = Logger.getLogger(UrbanProDataAnalysis2.class);

	@BeforeTest
	public void beforeMethod() {
		urbanPro = UrbanProProperties.getProperties();
		url = urbanPro.getProperty("offline_pune_url");
		System.setProperty("webdriver.firefox.marionette",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-UrbanPro-Leads/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(url);
	}

	@Test(priority = 0, enabled = true)
	public void findNumberOfPunePages() {
		paginate = driver.findElement(By.className("paginateButtonsnew"));
		aStep = paginate.findElements(By.cssSelector("a.step"));
		String noOfPunePages = aStep.get(aStep.size() - 1).getText();
		iPunePages = Integer.parseInt(noOfPunePages);
		logger.info("iPunePages------------------------->" + iPunePages);
	}

	@Test(priority = 1, enabled = true)
	public void getCurrentPageDetails() {

		for (int i = 1; i < 2; i++) {

			WebElement we1 = driver.findElement(By.className("paginateButtonsnew"));

			String linkText = we1.findElement(By.linkText((i + 1) + "")).getText();
			logger.info("Current Page No.=====================================>" + (i));
			logger.info("Next Page No (linkText).-------------------->" + linkText);
			fetchStudentsRequirementFromCurrentPage(i);

			// get the next link by text and click it.
			we1.findElement(By.linkText((i + 1) + "")).click();
		}

		displayPuneRequirements();
	}

	private boolean isValidOpenReqirementf2f(String s) {

		boolean bool = false;

		if (((s.toUpperCase().indexOf("Aundh".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Baner".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Chinchwad".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Hinjewadi".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Hinjawadi".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Infotech".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Sangavi".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Pimple".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Wakad".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Balewadi".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Pashan".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Thergaon".toUpperCase()) >= 0)
				|| (s.toUpperCase().indexOf("Ravet".toUpperCase()) >= 0))
				&& (s.toUpperCase().indexOf("requirementStatus:open".toUpperCase()) >= 0)) {
			bool = true;
		} else
			bool = false;

		return bool;
	}

	private boolean isValidOpenReqirementOnline(String s) {

		boolean bool = false;

		if ((s.toUpperCase().indexOf("Online".toUpperCase()) >= 0)
				&& (s.toUpperCase().indexOf("requirementStatus:open".toUpperCase()) >= 0)) {
			bool = true;
		} else
			bool = false;

		return bool;
	}

	private void processData(List<String> total) {

		List<String> processed = new ArrayList<String>();
		List<String> online = new ArrayList<String>();

		Iterator<String> iter = total.iterator();

		while (iter.hasNext()) {
			String s = iter.next();
			if (isValidOpenReqirementf2f(s)) {
				processed.add(s);
				iter.remove();
			} else if (isValidOpenReqirementOnline(s)) {
				online.add(s);
				iter.remove();
			}
		}

		logger.info("");
		logger.info("######################################processed######################################>"
				+ processed.size());
		// logger.info(processed);
		for (String s : processed) {
			logger.info(s);
		}
		logger.info(
				"######################################online######################################>" + online.size());
		for (String s : online) {
			logger.info(s);
		}
		logger.info("######################################remaining from total######################################>"
				+ total.size());
		for (String s : total) {
			logger.info(s);
		}

	}

	private void displayPuneRequirements() {
		/*
		 * Collections.sort(java); Collections.sort(selenium);
		 * Collections.sort(excel); Collections.sort(cLanguage);
		 * Collections.sort(cpp); Collections.sort(xml);
		 * Collections.sort(linux); Collections.sort(sql);
		 * Collections.sort(dotNet); Collections.sort(hadoop);
		 * Collections.sort(spring); Collections.sort(hibernate);
		 * Collections.sort(unix); Collections.sort(softwareTesting);
		 * Collections.sort(plSQL); Collections.sort(bigData);
		 * Collections.sort(angularJs); Collections.sort(mobile);
		 * Collections.sort(javaScript); Collections.sort(webDevelopment);
		 * Collections.sort(others);
		 */

		logger.info("java------------------->" + java.size());
		processData(java);

		logger.info("selenium--------------->" + selenium.size());
		processData(selenium);

		logger.info("excel------------------>" + excel.size());
		processData(excel);

		logger.info("cLanguage------------------>" + cLanguage.size());
		processData(cLanguage);

		logger.info("cpp--------------------->" + cpp.size());
		processData(cpp);

		logger.info("xml------------------>" + xml.size());
		processData(xml);

		logger.info("linux---------------->" + linux.size());
		processData(linux);

		logger.info("sql------------------>" + sql.size());
		processData(sql);

		logger.info("dotNet--------------->" + dotNet.size());
		processData(dotNet);

		logger.info("hadoop--------------->" + hadoop.size());
		processData(hadoop);

		logger.info("spring-------------->" + spring.size());
		processData(spring);

		logger.info("hibernate------------------->" + hibernate.size());
		processData(hibernate);

		logger.info("softwareTesting----------------->" + softwareTesting.size());
		processData(softwareTesting);

		logger.info("plSQL----------------->" + plSQL.size());
		processData(plSQL);

		logger.info("bigData--------------->" + bigData.size());
		processData(bigData);

		logger.info("angularJs---------------->" + angularJs.size());
		processData(angularJs);

		logger.info("mobile---------------->" + mobile.size());
		processData(mobile);

		logger.info("automation------------------->" + automation.size());
		processData(automation);

		logger.info("java script------------------->" + javaScript.size());
		processData(javaScript);

		logger.info("webDevelopment--------------->" + webDevelopment.size());
		processData(webDevelopment);

		logger.info("others--------------->" + others.size());
		processData(others);

	}

	private void fetchStudentsRequirementFromCurrentPage(int pageNo) {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("UrbanPro");
		int rowCount = 0;

		try {

			List<WebElement> l = driver.findElements(By.className("enquiry-card"));
			for (WebElement we : l) {

				WebElement ecDetailBlock = we.findElement(By.className("ec-detail-block"));
				List<WebElement> nameLocation = ecDetailBlock.findElements(By.tagName("p"));

				String studentName = nameLocation.get(1).getText();
				String studentLocation = nameLocation.get(2).getText();

				WebElement listWe2 = we.findElement(By.className("ch-c"));
				// for (WebElement we2 : listWe2) {
				List<WebElement> listWe3 = listWe2.findElements(By.cssSelector("a.redLink"));

				WebElement ec_category = listWe2.findElement(By.cssSelector("p.ec-category"));
				String ec_category_text = ec_category.getText();
				logger.debug("ec_category_text--------------->" + ec_category_text);

				/*
				 * WebElement we = divs.get(1).findElement(By.tagName("p"));
				 * List<WebElement> spans = we.findElements(By.tagName("span"));
				 * for(WebElement span:spans){
				 * System.out.println(span.getText()); }
				 */
				String requirementStatus = "open";
				List<WebElement> spans = ec_category.findElements(By.tagName("span"));
				for (WebElement span : spans) {
					requirementStatus = span.getText();
					logger.debug("requirementStatus------------------->" + requirementStatus);
				}

				

				for (WebElement we3 : listWe3) {
					String requirement = we3.getText();
					if (requirement != null && requirement.length() > 0) {
						Row row = sheet.createRow(++rowCount);

						Cell cell = row.createCell(1);
						cell.setCellValue(pageNo);

						Cell cell2 = row.createCell(2);
						cell2.setCellValue(studentLocation);

						Cell cell3 = row.createCell(3);
						cell3.setCellValue(requirement);

						Cell cell4 = row.createCell(4);
						cell4.setCellValue(requirementStatus);

						Cell cell5 = row.createCell(5);
						cell5.setCellValue(studentName);

						/*
						 * saveRequirementsInDataStructures(pageNo,
						 * studentLocation, requirement, requirementStatus,
						 * studentName);
						 */
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String excelFilePath = "UP.xlsx";
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(excelFilePath);
			workbook.write(outputStream);
			outputStream.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * private void fetchStudentsRequirementWithLocationFromCurrentPage() {
	 * List<WebElement> l = driver.findElements(By.className("enquiry-card"));
	 * for (WebElement we : l) { WebElement ecDetailBlock = we.findElement(By
	 * .className("ec-detail-block")); List<WebElement> nameLocation =
	 * ecDetailBlock.findElements(By .tagName("p"));
	 * 
	 * logger.info("Name---->" + nameLocation.get(1).getText());
	 * logger.info("Location---->" + nameLocation.get(2).getText()); } }
	 */

	private void saveRequirementsInDataStructures(int pageNo, String studentLocation, String requirement,
			String requirementStatus, String studentName) {

		if ((requirement.toUpperCase().indexOf("java".toUpperCase()) >= 0)
				&& (requirement.toUpperCase().indexOf("Java Script".toUpperCase()) < 0)
				&& (requirement.toUpperCase().indexOf("Hibernate".toUpperCase()) < 0)) {
			java.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("selenium".toUpperCase()) >= 0) {
			selenium.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("excel".toUpperCase()) >= 0) {
			excel.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("C Language".toUpperCase()) >= 0) {
			cLanguage.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("C++".toUpperCase()) >= 0) {
			cpp.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("xml".toUpperCase()) >= 0) {
			xml.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("linux".toUpperCase()) >= 0) {
			linux.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("sql".toUpperCase()) >= 0) {
			sql.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf(".Net".toUpperCase()) >= 0) {
			dotNet.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("hadoop".toUpperCase()) >= 0) {
			hadoop.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("spring".toUpperCase()) >= 0) {
			spring.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("hibernate".toUpperCase()) >= 0) {
			hibernate.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("unix".toUpperCase()) >= 0) {
			unix.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("Software Testing".toUpperCase()) >= 0) {
			softwareTesting.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("PL/SQL".toUpperCase()) >= 0) {
			plSQL.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if ((requirement.toUpperCase().indexOf("Big Data".toUpperCase()) >= 0)
				|| (requirement.toUpperCase().indexOf("MongoDB".toUpperCase()) >= 0)) {
			bigData.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("angular".toUpperCase()) >= 0) {
			angularJs.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("mobile".toUpperCase()) >= 0) {
			mobile.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("Automation".toUpperCase()) >= 0) {
			automation.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("Java Script".toUpperCase()) >= 0) {
			javaScript.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else if (requirement.toUpperCase().indexOf("Web Development".toUpperCase()) >= 0) {
			webDevelopment.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:"
					+ requirementStatus + ",  Location:" + studentLocation + "  " + requirement);
		} else {
			others.add("pageNo:" + pageNo + " studentName:" + studentName + "  requirementStatus:" + requirementStatus
					+ ",  Location:" + studentLocation + "  " + requirement);
		}
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	@AfterMethod
	public void afterMethod() {
	}

}
