package com.c2t.leads.urbanpro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.apachepoi.write.Book;
import com.c2t.leads.data.UrbanProData;
import com.c2t.utils.DateUtils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;

public class UrbanProDataAnalysis3 {

	WebDriver driver = null;
	List<WebElement> aStep = null;
	WebElement paginate = null;
	WebDriverWait wait = null;
	int iPunePages;
	int rowCount = 0;

	static String url;
	static Properties urbanPro;

	XSSFWorkbook workbook = null;
	XSSFSheet sheet = null;
	List studentDetails = null;

	final static Logger logger = Logger.getLogger(UrbanProDataAnalysis3.class);

	@BeforeTest
	public void beforeMethod1() {
		urbanPro = UrbanProProperties.getProperties();
		url = urbanPro.getProperty("pune_selenium");
		System.setProperty("webdriver.firefox.marionette",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-UrbanPro-Leads/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(url);
	}

	@BeforeTest
	public void beforeMethod2() {
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet(DateUtils.getDateDdMmYyyy());
		studentDetails = new ArrayList();
	}

	@Test(priority = 0, enabled = true)
	public void findNumberOfPunePages() {
		paginate = driver.findElement(By.className("paginateButtonsnew"));
		aStep = paginate.findElements(By.cssSelector("a.step"));
		String noOfPunePages = aStep.get(aStep.size() - 1).getText();
		iPunePages = Integer.parseInt(noOfPunePages);
		logger.info("iPunePages------------------------->" + iPunePages);
	}

	@Test(priority = 1, enabled = true)
	public void getCurrentPageDetails() {

		

		for (int i = 1; i < 2; i++) {

			WebElement we1 = driver.findElement(By.className("paginateButtonsnew"));

			String linkText = we1.findElement(By.linkText((i + 1) + "")).getText();
			logger.info("Current Page No.=====================================>" + (i));
			logger.info("Next Page No (linkText).-------------------->" + linkText);
			fetchStudentsRequirementFromCurrentPage(i, studentDetails, sheet);

			// get the next link by text and click it.
			we1.findElement(By.linkText((i + 1) + "")).click();
		}

	}

	private void fetchStudentsRequirementFromCurrentPage(int pageNo, List studentDetails, XSSFSheet sheet) {

		try {

			List<WebElement> l = driver.findElements(By.className("enquiry-card"));
			for (WebElement we : l) {

				WebElement ecDetailBlock = we.findElement(By.className("ec-detail-block"));
				List<WebElement> nameLocation = ecDetailBlock.findElements(By.tagName("p"));

				String studentName = nameLocation.get(1).getText();
				String studentLocation = nameLocation.get(2).getText();

				WebElement listWe2 = we.findElement(By.className("ch-c"));
				// for (WebElement we2 : listWe2) {
				List<WebElement> listWe3 = listWe2.findElements(By.cssSelector("a.redLink"));

				WebElement ec_category = listWe2.findElement(By.cssSelector("p.ec-category"));
				String ec_category_text = ec_category.getText();
				logger.debug("ec_category_text--------------->" + ec_category_text);

				/*
				 * WebElement we = divs.get(1).findElement(By.tagName("p"));
				 * List<WebElement> spans = we.findElements(By.tagName("span"));
				 * for(WebElement span:spans){
				 * System.out.println(span.getText()); }
				 */
				String requirementStatus = "";
				List<WebElement> spans = ec_category.findElements(By.tagName("span"));
				for (WebElement span : spans) {
					requirementStatus = span.getText();
					logger.debug("requirementStatus------------------->" + requirementStatus);
				}

				for (WebElement we3 : listWe3) {
					String requirement = we3.getText();
					if (requirement != null && requirement.length() > 0) {

						UrbanProData data = new UrbanProData();
						data.setPageNo(pageNo);
						data.setStudentLocation(studentLocation);
						data.setRequirement(requirement);
						data.setRequirementStatus(requirementStatus);
						data.setStudentName(studentName);
						
						sheet.autoSizeColumn(1);
						sheet.autoSizeColumn(2);

						Row row = sheet.createRow(rowCount);
						int cellCount = -1;

						Cell cell0 = row.createCell(++cellCount);
						cell0.setCellValue(pageNo);

						Cell cell1 = row.createCell(++cellCount);
						cell1.setCellValue(studentLocation);

						Cell cell2 = row.createCell(++cellCount);
						cell2.setCellValue(requirement);

						Cell cell3 = row.createCell(++cellCount);
						cell3.setCellValue(requirementStatus);

						Cell cell4 = row.createCell(++cellCount);
						cell4.setCellValue(studentName);
						
						++rowCount;

						/*
						 * saveRequirementsInDataStructures(pageNo,
						 * studentLocation, requirement, requirementStatus,
						 * studentName);
						 */
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * private void fetchStudentsRequirementWithLocationFromCurrentPage() {
	 * List<WebElement> l = driver.findElements(By.className("enquiry-card"));
	 * for (WebElement we : l) { WebElement ecDetailBlock = we.findElement(By
	 * .className("ec-detail-block")); List<WebElement> nameLocation =
	 * ecDetailBlock.findElements(By .tagName("p"));
	 * 
	 * logger.info("Name---->" + nameLocation.get(1).getText());
	 * logger.info("Location---->" + nameLocation.get(2).getText()); } }
	 */

	@AfterTest
	public void afterTest() {
		driver.quit();

		String excelFilePath =  DateUtils.getDateDdMmYyyyHhMmSs() +  " UP.xlsx";
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(excelFilePath);
			workbook.write(outputStream);
			outputStream.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void afterMethod() {
	}

}
