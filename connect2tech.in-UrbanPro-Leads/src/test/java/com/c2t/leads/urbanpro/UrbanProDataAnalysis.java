package com.c2t.leads.urbanpro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.c2t.apachepoi.write.Book;
import com.c2t.leads.data.UrbanProData;
import com.c2t.utils.DateUtils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;

public class UrbanProDataAnalysis {

	WebDriver driver = null;
	List<WebElement> aStep = null;
	WebElement paginate = null;
	WebDriverWait wait = null;
	int iPunePages;
	int rowCount = 0;
	String excelFilePath = null;

	static String url;
	static Properties urbanPro;

	XSSFWorkbook workbook = null;
	XSSFSheet sheet = null;
	List studentDetails = null;

	final static Logger logger = Logger.getLogger(UrbanProDataAnalysis.class);

	@BeforeTest
	public void beforeMethod1() {
		urbanPro = UrbanProProperties.getProperties();

		//String url = urbanPro.getProperty("pune_python");
		url = urbanPro.getProperty("pune_python");
		excelFilePath = "fetch_pune_python";

		/*String fetch = "online_python";
		//if (is_java_online != null && !is_java_online.equals("") && is_java_online.equals("true")) {
			url = urbanPro.getProperty(fetch);
			excelFilePath = fetch+"_";
		//}
*/
		System.setProperty("webdriver.firefox.marionette",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-UrbanPro-Leads/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get(url);
	}

	@BeforeTest
	public void beforeMethod2() {
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet(DateUtils.getDateDdMmYyyy());
		studentDetails = new ArrayList();
	}

	@Test(priority = 0, enabled = true)
	public void findNumberOfPunePages() {
		paginate = driver.findElement(By.className("paginateButtonsnew"));
		aStep = paginate.findElements(By.cssSelector("a.step"));
		String noOfPunePages = aStep.get(aStep.size() - 1).getText();
		iPunePages = Integer.parseInt(noOfPunePages);
		logger.info("iPunePages------------------------->" + iPunePages);
	}

	@Test(priority = 1, enabled = true)
	public void getCurrentPageDetails() {

		for (int i = 1; i < 3; i++) {

			WebElement we1 = driver.findElement(By.className("paginateButtonsnew"));

			String linkText = we1.findElement(By.linkText((i + 1) + "")).getText();
			logger.info("Current Page No.=====================================>" + (i));
			logger.info("Next Page No (linkText).-------------------->" + linkText);
			fetchStudentsRequirementFromCurrentPage(i, studentDetails, sheet);

			// get the next link by text and click it.
			we1.findElement(By.linkText((i + 1) + "")).click();
		}

	}

	private void fetchStudentsRequirementFromCurrentPage(int pageNo, List studentDetails, XSSFSheet sheet) {

		try {

			List<WebElement> l = driver.findElements(By.className("enquiry-card"));
			for (WebElement we : l) {

				WebElement customerDtls = we.findElement(By.className("ec-detail-block"));
				List<WebElement> paraCustomerDtls = customerDtls.findElements(By.tagName("p"));

				String studentName = paraCustomerDtls.get(1).getText();
				String studentLocation = paraCustomerDtls.get(2).getText();
				String studentPhone = paraCustomerDtls.get(3).getText();
				logger.debug("studentName--------------->" + studentName);
				logger.debug("studentLocation--------------->" + studentLocation);
				logger.debug("studentPhone--------------->" + studentPhone);

				WebElement customerLocationPref = we.findElement(By.className("mar-left"));
				List<WebElement> paraCustomerLocationPref = customerLocationPref.findElements(By.tagName("p"));

				//Travel, Home, Online
				String pref1 = "";
				String pref2 = "";
				String pref3 = "";

				for(int i=0;i<paraCustomerLocationPref.size();i++){
					String val = paraCustomerLocationPref.get(i).getText();
					
					if(val!=null && !val.equals("")){
						val = val.toUpperCase(); 
						if(val.contains("TRAVEL")){
							pref1 = val;
						}else if(val.contains("ONLINE")){
							pref2 = val;
						}else if(val.contains("HOME")){
							pref3 = val;
						}
					}
					
				}
				
				WebElement courseName = we.findElement(By.className("fontSize13"));
				WebElement course = courseName.findElement(By.tagName("h3"));
				String courseText = course.getText();
				
				WebElement categoryTimeWE = we.findElement(By.cssSelector("p.ec-category"));
				String categoryTime = categoryTimeWE.getText();
				
				String status = "Open";
				
				List<WebElement> spans = categoryTimeWE.findElements(By.tagName("span"));
				logger.debug("spans--------------->" + spans);
				if(spans.size()>0){
					status = spans.get(0).getText();
				}
				
				logger.debug("pref1--------------->" + pref1);
				logger.debug("pref2--------------->" + pref2);
				logger.debug("pref3--------------->" + pref3);

				Row row = sheet.createRow(rowCount);
				int cellCount = -1;
				
				Cell cell0 = row.createCell(++cellCount);
				cell0.setCellValue(pageNo);

				sheet.autoSizeColumn(++cellCount);
				Cell courseTextCell = row.createCell(cellCount);
				courseTextCell.setCellValue(courseText);
				
				sheet.autoSizeColumn(++cellCount);
				Cell categoryTimeCell = row.createCell(cellCount);
				categoryTimeCell.setCellValue(categoryTime);
				
				//Status
				sheet.autoSizeColumn(++cellCount);
				Cell statusCell = row.createCell(cellCount);
				statusCell.setCellValue(status);
				
				sheet.autoSizeColumn(++cellCount);
				Cell cell1 = row.createCell(cellCount);
				cell1.setCellValue(studentName);
				
				sheet.autoSizeColumn(++cellCount);
				Cell cell2 = row.createCell(cellCount);
				cell2.setCellValue(studentLocation);

				sheet.autoSizeColumn(++cellCount);
				Cell cell3 = row.createCell(cellCount);
				cell3.setCellValue(studentPhone);

				sheet.autoSizeColumn(++cellCount);
				Cell cell4 = row.createCell(cellCount);
				cell4.setCellValue(pref1);

				sheet.autoSizeColumn(++cellCount);
				Cell cell5 = row.createCell(cellCount);
				cell5.setCellValue(pref2);

				sheet.autoSizeColumn(++cellCount);
				Cell cell6 = row.createCell(cellCount);
				cell6.setCellValue(pref3);
				
				++rowCount;

				/*
				 * WebElement listWe2 = we.findElement(By.className("ch-c"));
				 * List<WebElement> listWe3 =
				 * listWe2.findElements(By.cssSelector("a.redLink"));
				 * 
				 * WebElement ec_category =
				 * listWe2.findElement(By.cssSelector("p.ec-category")); String
				 * ec_category_text = ec_category.getText();
				 * logger.debug("ec_category_text--------------->" +
				 * ec_category_text);
				 */

				/*
				 * WebElement we = divs.get(1).findElement(By.tagName("p"));
				 * List<WebElement> spans = we.findElements(By.tagName("span"));
				 * for(WebElement span:spans){
				 * System.out.println(span.getText()); }
				 */
				/*
				 * String requirementStatus = ""; List<WebElement> spans =
				 * ec_category.findElements(By.tagName("span")); for (WebElement
				 * span : spans) { requirementStatus = span.getText();
				 * logger.debug("requirementStatus------------------->" +
				 * requirementStatus); }
				 * 
				 * for (WebElement we3 : listWe3) { String requirement =
				 * we3.getText(); if (requirement != null &&
				 * requirement.length() > 0) {
				 * 
				 * UrbanProData data = new UrbanProData();
				 * data.setPageNo(pageNo);
				 * data.setStudentLocation(studentLocation);
				 * data.setRequirement(requirement);
				 * data.setRequirementStatus(requirementStatus);
				 * data.setStudentName(studentName);
				 * 
				 * sheet.autoSizeColumn(1); sheet.autoSizeColumn(2);
				 * 
				 * Row row = sheet.createRow(rowCount); int cellCount = -1;
				 * 
				 * Cell cell0 = row.createCell(++cellCount);
				 * cell0.setCellValue(pageNo);
				 * 
				 * Cell cell1 = row.createCell(++cellCount);
				 * cell1.setCellValue(studentLocation);
				 * 
				 * Cell cell2 = row.createCell(++cellCount);
				 * cell2.setCellValue(requirement);
				 * 
				 * Cell cell3 = row.createCell(++cellCount);
				 * cell3.setCellValue(requirementStatus);
				 * 
				 * Cell cell4 = row.createCell(++cellCount);
				 * cell4.setCellValue(studentName);
				 * 
				 * ++rowCount;
				 * 
				 * 
				 * saveRequirementsInDataStructures(pageNo, studentLocation,
				 * requirement, requirementStatus, studentName);
				 * 
				 * }
				 * 
				 * }
				 */
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * private void fetchStudentsRequirementWithLocationFromCurrentPage() {
	 * List<WebElement> l = driver.findElements(By.className("enquiry-card"));
	 * for (WebElement we : l) { WebElement ecDetailBlock = we.findElement(By
	 * .className("ec-detail-block")); List<WebElement> nameLocation =
	 * ecDetailBlock.findElements(By .tagName("p"));
	 * 
	 * logger.info("Name---->" + nameLocation.get(1).getText());
	 * logger.info("Location---->" + nameLocation.get(2).getText()); } }
	 */

	@AfterTest
	public void afterTest() {
		driver.quit();

		excelFilePath = excelFilePath + DateUtils.getDateDdMmYyyyHhMmSs() + " UP.xlsx";
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(excelFilePath);
			workbook.write(outputStream);
			outputStream.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void afterMethod() {
	}

}
