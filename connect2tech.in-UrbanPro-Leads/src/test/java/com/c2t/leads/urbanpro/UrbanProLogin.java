package com.c2t.leads.urbanpro;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class UrbanProLogin {
	WebDriver driver = null;

	@BeforeTest
	public void beforeMethod1() {

		System.setProperty("webdriver.firefox.marionette",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-UrbanPro-Leads/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.urbanpro.com/login");
	}
	
	@Test
	public void test1(){
		driver.findElement(By.id("j_username")).sendKeys("message4naresh@gmail.com");
		driver.findElement(By.cssSelector("input.newBtn")).click();
		driver.findElement(By.id("j_password")).sendKeys("Automation12#$");
		
		/*WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(driver
				.findElement(By.cssSelector("input.newBtn"))));*/
		
		driver.findElement(By.cssSelector(".newBtn.blueBtn.mt-15.submitBtn.recaptchSubmit")).click();
	}

}
