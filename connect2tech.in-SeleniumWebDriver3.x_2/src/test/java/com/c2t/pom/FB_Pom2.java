package com.c2t.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FB_Pom2 {
	WebDriver d;

	@FindBy(name = "email")
	WebElement emailWeb;

	@FindBy(name = "pass1")
	WebElement passwordWeb;

	@FindBy(id = "loginbutton")
	WebElement bottonWeb;

	FB_Pom2(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	public void setMail(String email) {
		System.out.println("emailWeb=>" + emailWeb);
		emailWeb.sendKeys(email);

	}

	public void setPassword(String password) {
		System.out.println("passwordWeb=>" + passwordWeb);
		passwordWeb.sendKeys(password);

	}

	public void clickLogin() {

		bottonWeb.click();
	}

	public void login(String s1, String s2) {
		setMail(s1);
		setPassword(s2);
		clickLogin();
	}

}
