package com.c2t.pom;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.pom.Guru99HomePage;
import com.c2t.pom.LoginPOM;

public class Test99GuruLogin2 {
	WebDriver driver;
	LoginPOM2 pom;

	@BeforeTest

	void setup() {
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/chromedriver_win32_2.46/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");
		
		pom = new LoginPOM2(driver);
	}

	@Test(priority = 0)
	public void verifyTitle() {
		
		String expected = "Login";
		String actualValue = pom.verifyTitle();
		
		Assert.assertEquals(actualValue, expected);
	}

	@Test(priority = 1)
	public void login() {
		
		pom.setEmail("email@gmail.com");
		pom.setPassword("password");
		pom.clickLogin();
		
		
	}

}