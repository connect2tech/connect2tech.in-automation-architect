package com.c2t.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FB_Pom {
	WebDriver driver;
	WebElement emailWeb;
	WebElement passwordWeb;
	
	FB_Pom(WebDriver d){
		driver = d;
	}
	
	public void setUserNameId(String id){
		emailWeb = driver.findElement(By.name("email"));
		emailWeb.sendKeys(id);
	}
	
	public void setPwd(String pwd){
		passwordWeb = driver.findElement(By.name("pass"));
		passwordWeb.sendKeys(pwd);
	}
	
}
