package com.c2t.listeners;

import org.openqa.selenium.WebDriver;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Singleton {

	static Singleton singleton;
	
	WebDriver d;
	

	private Singleton() {
	}
	
	

	public WebDriver getD() {
		return d;
	}



	public void setD(WebDriver d) {
		this.d = d;
	}



	public static Singleton getSingleton() {
		if (singleton == null) {
			singleton = new Singleton();
			return singleton;
		} else {
			return singleton;
		}
	}
}
