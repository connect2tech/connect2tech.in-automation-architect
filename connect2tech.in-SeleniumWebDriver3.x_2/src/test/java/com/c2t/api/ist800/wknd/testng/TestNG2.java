package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Test;

public class TestNG2 {
	
	@Test(priority=-1)
	public void createFbAccount(){
		System.out.println("createFbAccount");
	}

	@Test(priority=1)
	public void openFbUrl_1() {
		System.out.println("test3");
	}
	
	@Test(priority=1)
	public void openFbUrl_2() {
		System.out.println("test3");
	}

	@Test(priority=2)
	public void enterUserNamePwd() {
		System.out.println("test1");
	}

	@Test(priority=3)
	public void clickLoginButton() {
		System.out.println("test2");
	}

}
