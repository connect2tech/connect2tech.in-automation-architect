package com.c2t.api.ist800.wknd.testng;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestNG_Assertion {

	@Test
	public void login() {

		// Logic.
		String expectedVal = "Welcome";
		String actualVal = "Welcome1";

		Assert.assertEquals(actualVal, expectedVal, "The login failed as expected and actual values are not same");

	}

}
