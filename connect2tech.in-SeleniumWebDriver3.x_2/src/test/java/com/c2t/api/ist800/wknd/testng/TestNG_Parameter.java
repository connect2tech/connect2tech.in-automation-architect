package com.c2t.api.ist800.wknd.testng;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestNG_Parameter {

	@Parameters({ "expValue", "OS" })
	@Test
	public void login(String val1, String val2) {

		// Logic.
		System.out.println(val1);
		System.out.println(val2);

		if (val2.equals("windows")) {

		} else {

		}

	}

}
