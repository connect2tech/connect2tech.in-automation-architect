package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Test;

public class TestNG_Threads2 {

	@Test
	public void createFbAccount() {
		System.out.println("createFbAccount");
		System.out.println("Thread2/id2="+Thread.currentThread().getId());
	}

	@Test
	public void openFbUrl_1() {
		System.out.println("test3");
		System.out.println("Thread2/id1="+Thread.currentThread().getId());
	}

	@Test
	public void openFbUrl_2() {
		System.out.println("test3");
		System.out.println("Thread2/id3="+Thread.currentThread().getId());
	}

	@Test
	public void enterUserNamePwd() {
		System.out.println("test1");
		System.out.println("Thread2/id4="+Thread.currentThread().getId());
	}

	@Test
	public void clickLoginButton() {
		System.out.println("test2");
		System.out.println("Thread2/id5="+Thread.currentThread().getId());
	}

}
