package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNG100 {
	
	@BeforeTest
	public void method1(){
		System.out.println("method1");
	}
	
	@BeforeMethod
	public void beforeMethod(){
		System.out.println("-----------------beforeMethod-------------");
	}

	@Test
	public void test3() {
		System.out.println("test3");
	}

	@Test
	public void test1() {
		System.out.println("test1");
	}

	@Test
	public void test2() {
		System.out.println("test2");
	}
	
	@AfterMethod
	public void afterMethod(){
		System.out.println("-----------------afterMethod-------------");
	}
	
	@AfterTest
	public void after(){
		System.out.println("After");
	}

}
