package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Test;

public class TestNG_Threads {

	@Test
	public void createFbAccount() {
		System.out.println("createFbAccount");
		System.out.println("id2="+Thread.currentThread().getId());
	}

	@Test
	public void openFbUrl_1() {
		System.out.println("test3");
		System.out.println("id1="+Thread.currentThread().getId());
	}

	@Test
	public void openFbUrl_2() {
		System.out.println("test3");
		System.out.println("id3="+Thread.currentThread().getId());
	}

	@Test
	public void enterUserNamePwd() {
		System.out.println("test1");
		System.out.println("id4="+Thread.currentThread().getId());
	}

	@Test
	public void clickLoginButton() {
		System.out.println("test2");
		System.out.println("id5="+Thread.currentThread().getId());
	}

}
