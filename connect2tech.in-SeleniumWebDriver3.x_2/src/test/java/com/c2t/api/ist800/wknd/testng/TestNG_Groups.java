package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Test;

public class TestNG_Groups {

	@Test(groups={"regression"})
	public void test3() {
		System.out.println("test3");
	}

	@Test(groups={"load"})
	public void test1() {
		System.out.println("test1");
	}

	@Test(groups={"regression", "load"})
	public void test2() {
		System.out.println("test2");

	}

}
