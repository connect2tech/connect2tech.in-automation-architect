package com.c2t.testng.basic;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNG1 {
	
	
	@Test(priority=-1)
	public void firstname(){
		System.out.println("test1");
		
		//System.out.println(Thread.currentThread().getId());
	}
	
	@Test(priority=2)
	public void middlename(){
		System.out.println("test2");
		//System.out.println(Thread.currentThread().getId());
	}
	
	@Test(priority=3)
	public void lastname(){
		System.out.println("test3");
		//System.out.println(Thread.currentThread().getId());
	}
	
}
