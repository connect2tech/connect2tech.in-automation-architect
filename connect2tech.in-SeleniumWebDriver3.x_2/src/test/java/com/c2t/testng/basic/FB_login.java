package com.c2t.testng.basic;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FB_login {
	
	@DataProvider
	public Object[][] data(){
		
		String [][] arr = {
				{"user1","pwd1", "pass"},
				{"user2","pwd2","fail"}
		};
		
		//Read from excel sheet.
		
		return arr;
		
	}
	
	
	
	@Test(dataProvider="data")
	public void login(String name , String password, String exp) {
		
		System.out.println(name);
		System.out.println(password);
		
		Assert.assertEquals("", exp);
		
	}
	
	

	
	
	

}
