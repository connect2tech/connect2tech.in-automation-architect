package com.c2t.ist730;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;

import org.testng.annotations.*;

import javafx.scene.chart.PieChart.Data;

import static org.testng.Assert.*;

public class DDT_Class {

	private WebDriver driver;

	@BeforeTest
	public void setUp() {
		/*
		 * System.setProperty("webdriver.chrome.driver",
		 * "D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe"
		 * ); driver = new ChromeDriver();
		 * driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");
		 */
	}

	@DataProvider
	public String[][] Data() {

		String data[][] = new String[][] {

				{ "160", "45", "17.6", "Underweight" },

				{ "165", "50", "18.4", "Underweight" },

				{ "170", "50", "17.3", "Underweight" }

		};

		return data;
	}

	@Test(dataProvider = "Data")
	public void testBMICalculator(String col1, String col2, String col3, String col4) {
		System.out.println(col1);
		System.out.println(col2);
		System.out.println(col3);
		System.out.println(col4);

	}

}
