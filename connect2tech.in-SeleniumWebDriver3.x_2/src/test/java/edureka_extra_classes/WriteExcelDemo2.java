package edureka_extra_classes;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import statements
public class WriteExcelDemo2 {
	public static void main(String[] args) throws Exception{

		// XSSFWorkbook workbook = new XSSFWorkbook();
		// XSSFSheet sheet = workbook.createSheet("Employee Data");
		// Row row = sheet.createRow(rowCount);
		// Cell cell = row.createCell(colCount);
		// cell.setCellValue(data[rowCount][colCount]);
		// FileOutputStream out = new FileOutputStream(new
		// File("howtodoinjava_out.xlsx"));
		// workbook.write(out);
		// out.close();

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("WriteToExcel");

		XSSFRow row0 = sheet.createRow(0);

		row0.createCell(0).setCellValue("Hello");
		row0.createCell(1).setCellValue("Hello1");

		XSSFRow row1 = sheet.createRow(1);

		row1.createCell(0).setCellValue("Hello");
		row1.createCell(1).setCellValue("Hello1");
		
		File f = new File("NewFile.xlsx");
		FileOutputStream fos = new FileOutputStream(f);
		workbook.write(fos);
	}
}