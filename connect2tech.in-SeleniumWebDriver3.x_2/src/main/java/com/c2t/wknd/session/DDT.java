package com.c2t.wknd.session;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class DDT {

	private WebDriver driver;
	
	static Logger logger ;

	@BeforeTest
	public void setUp() {
		logger = Logger.getLogger(DDT.class);
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/chromedriver_win32_2.46/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://cookbook.seleniumacademy.com/bmicalculator.html");

	}
	
	
	public String[][] readDataFromExcel() throws Exception {

		String myData[][] = new String[2][4];

		File file = new File("DDT.xlsx");
		FileInputStream fileInputStream = new FileInputStream(file);

		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = workbook.getSheet("Employee Data");

		Iterator<Row> rows = sheet.rowIterator();

		int rowCount = 0;

		while (rows.hasNext()) {

			Row row = rows.next();

			Iterator<Cell> cells = row.cellIterator();

			int colCount = 0;
			while (cells.hasNext()) {

				Cell cellValue = cells.next();
				//System.out.println("cellValue=" + cellValue);

				myData[rowCount][colCount] = cellValue.toString();
				++colCount;
				
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				logger.info("value from excel is  : " + cellValue.toString());
				

			}

			++rowCount;

			System.out.println("------------------------------------");

		}

		return myData;

	}

	/*
	 * Mark a method as supplying data for a test method. The data provider name
	 * defaults to method name. The annotated method must return an Object[][]
	 * where each Object[] can be assigned the parameter list of the test
	 * method. The @Test method that wants to receive data from this
	 * DataProvider needs to use a dataProvider name equals to the name of this
	 * annotation.
	 */
	@DataProvider
	public String[][] testData() throws Exception{

		/*String data[][] = new String[][] {

				{ "160", "45", "17.6", "Underweight" },

				{ "165", "50", "18.4", "Underweight" },

				{ "170", "50", "17.3", "Underweight" }

		};*/
		
		String data[][] = readDataFromExcel();

		return data;
	}

	@Test(dataProvider = "testData")
	public void testBMICalculator(String height, String weight, String bmi, String category) {

		WebElement heightField = driver.findElement(By.name("heightCMS"));
		heightField.clear();
		heightField.sendKeys(height);

		WebElement weightField = driver.findElement(By.name("weightKg"));
		weightField.clear();
		weightField.sendKeys(weight);

		WebElement calculateButton = driver.findElement(By.id("Calculate"));
		calculateButton.click();

		WebElement bmiLabel = driver.findElement(By.name("bmi"));
		assertEquals(bmiLabel.getAttribute("value"), bmi);

		WebElement bmiCategoryLabel = driver.findElement(By.name("bmi_category"));
		assertEquals(bmiCategoryLabel.getAttribute("value"), category);
	}

}
