package com.c2t.miscellaneous;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.awt.image.BufferedImage;
import java.net.URL;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * This program fetche all images from given URL. Make sure that D:/ drive has
 * img folder with write permissions.
 * 
 * @author Naresh Chaurasia
 *
 */
public class DownloadImage {

	public static void main(String args[]) {

		String path = "C:/Users/naresh/Pictures/CodeImages/Eclipse";

		System.setProperty("webdriver.firefox.marionette", "geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("https://www.techbeamers.com/install-testng-in-eclipse-ide/");

		// Fetching all the images from the page
		List<WebElement> allImages = driver.findElements(By.tagName("img"));

		for (int m = 0; m < allImages.size(); m++) {

			String imgSrc = allImages.get(m).getAttribute("src");

			if (imgSrc != null && !imgSrc.equals("")) {

				String src = allImages.get(m).getAttribute("src");
				System.out.println(src);

				int lastDotIndex = src.lastIndexOf(".");
				String imgExt = src.substring(lastDotIndex + 1, src.length());

				int lastSlashIndex = src.lastIndexOf("/");
				String imgName = src.substring(lastSlashIndex + 1, src.length());

				try {
					URL url = new URL(allImages.get(m).getAttribute("src"));
					BufferedImage img = ImageIO.read(url.openStream());

					if (img != null) {
						imgName = "Eclipse-" + m;
						if (imgExt.equals("png")) {
							imgName = imgName + ".png";
							ImageIO.write(img, "png", new File(path + imgName));
						} else if (imgExt.equals("gif")) {
							imgName = imgName + ".gif";
							ImageIO.write(img, "gif", new File(path + imgName));
						} else if (imgExt.equals("jpg")) {
							imgName = imgName + ".jpg";
							ImageIO.write(img, "jpg", new File(path + imgName));
						} else if (imgExt.equals("svg")) {
							imgName = imgName + ".svg";
							ImageIO.write(img, "svg", new File(path + imgName));
						}

						System.out.println(imgName + " stored in D:/img folder");
					} else {
						System.out.println("Not able to read :: " + url);
					}

				} catch (Exception e) {
					System.out.println(e);
				}
			} else {
				System.out.println("The Image Src is null..");
			}
		}

		//driver.close();

	}
}
