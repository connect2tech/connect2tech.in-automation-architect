package com.c2t.edureka.wknd.mar19.ist20_30.selenium1;

import java.util.*;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ListInJava {
	public static void main(String[] args) {
		List <String> l = new ArrayList<String>();
		
		l.add("A");
		l.add("B");
		l.add("C");
		
		for(int i=0;	i<l.size();	i++){
			System.out.println(l.get(i));
		}
	}
}
