package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.mustache.StringChunk;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FbRegPom {
	
	WebDriver d;
	
	public FbRegPom(WebDriver driver){
		d = driver;
	}
	
	public void setEmail(String s1){
		d.findElement(By.name(s1)).sendKeys("naresh");
	}
	
	public void setPassword(String s2){
		d.findElement(By.id(s2)).sendKeys("naresh");
	}
	
	public void clickLogin(){
		d.findElement(By.id("loginbutton")).click();
		
	}
}
