package com.c2t.edureka.wknd.mar19.ist20_30.selenium6;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.openqa.selenium.chrome.ChromeDriver;

public class SikuliDemoChromeWorking2 {

	public static void main(String[] args) throws FindFailed {

		WebDriver driver;
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/chromedriver_win32_2.46/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/FileUpload.html");
		driver.findElement(By.id("fileToUpload")).click();
		
		
		Pattern textBox = new Pattern("D:/nchaurasia/Automation-Architect/img/Img-Text1.PNG");
		Pattern git = new Pattern("D:/nchaurasia/Automation-Architect/img/Git.PNG");
		
		Screen screen = new Screen();
		screen.type(textBox,"hello kimberly");
		screen.wait(git,5);
		screen.click(git);
		
		
	}

}
