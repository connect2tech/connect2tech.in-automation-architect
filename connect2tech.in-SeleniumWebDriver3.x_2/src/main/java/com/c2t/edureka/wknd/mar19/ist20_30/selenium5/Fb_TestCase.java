package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Fb_TestCase {

	WebDriver driver;
	FbLoginPom pom;

	@BeforeMethod
	public void before() {
		String url = "https://www.facebook.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.get(url);

		pom = new FbLoginPom(driver);
	}

	@Test
	public void test1() {
		pom.setEmail("user1");
		pom.setPassword("pass");
		pom.clickLogin();
	}

}