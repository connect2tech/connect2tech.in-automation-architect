package com.c2t.edureka.wknd.mar19.ist20_30.selenium4;

import java.util.HashMap;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Employee{
	String name;
	String company;
	int age;
	int salary;
}

public class HashMapExample {

	public static void main(String[] args) {
		HashMap<String, Employee> map = new HashMap<String, Employee>();
		
		Employee e1 = new Employee();
		e1.name="rahul";
		e1.company = "XYZ";
		e1.age = 25;
		e1.salary = 10000;
		
		Employee e2 = new Employee();
		e2.name="kishan";
		e2.company = "XYZ";
		e2.age = 25;
		e2.salary = 10000;
		
		map.put("emp100", e1);
		map.put("emp200", e2);
		
		Employee emp = map.get("emp200");
		
		
		
	}

}
