package com.c2t.edureka.wknd.mar19.ist20_30.selenium3;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ListExample {
	public static void main(String[] args) {
		
		int arr [] = new int[]{10,20,30};
		
		
		
		ArrayList <String> l = new ArrayList<String>();
		
		l.add("A");
		l.add("B");
		l.add("C");
		l.add("A");
		System.out.println(l);
		
	/*	for(int i=0;	i<l.size();	i++){
			System.out.println(l.get(i));
		}
		
		boolean b = l.contains("A");
		System.out.println(b);
		
		l.remove(0);*/
		
		
		Iterator <String> iter = l.iterator();
		
		while(iter.hasNext()){
			String s = iter.next();
			System.out.println(s);
		}
		
		
		
	}
}
