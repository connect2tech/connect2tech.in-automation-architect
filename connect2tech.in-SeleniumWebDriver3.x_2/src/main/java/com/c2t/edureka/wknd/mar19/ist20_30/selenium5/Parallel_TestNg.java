package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Parallel_TestNg {

	@Test
	public void test1() {
		System.out.println("id1="+Thread.currentThread().getId());
		System.out.println("first test");
		WebDriver driver = new ChromeDriver();
	}

	@Test
	public void test2() {
		System.out.println("id2="+Thread.currentThread().getId());
		System.out.println("second test");
		WebDriver driver = new ChromeDriver();
	}

}
