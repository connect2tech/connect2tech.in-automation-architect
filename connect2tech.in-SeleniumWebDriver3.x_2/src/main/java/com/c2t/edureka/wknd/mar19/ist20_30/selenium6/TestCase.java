package com.c2t.edureka.wknd.mar19.ist20_30.selenium6;

import java.util.Properties;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.c2t.framework.hybrid.ExcelFile;
import com.c2t.framework.hybrid.ReadObject;
import com.c2t.framework.hybrid.UIOperation;

public class TestCase {

	WebDriver webdriver;
	ExcelFile excelSheet;
	ReadObject object;
	Properties propFile;

	@BeforeTest
	public void beforeTest() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/chromedriver_win32_2.45/chromedriver.exe");
		webdriver = new ChromeDriver();

		excelSheet = new ExcelFile();
		object = new ReadObject();
		propFile = object.getPropertiesValues();
	}

	@Test
	public void test() throws Exception {
		UIOperation operation = new UIOperation();
		operation.setDriver(webdriver);

		Sheet sheet = excelSheet.readExcel("D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2",
				"TestCase.xlsx", "KeywordFramework");
		
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		
		for(int i=1;	i<=rowCount;	 i++){
			Row row = sheet.getRow(i);
			
			String val = row.getCell(0).toString();
			
			if(!val.equals("")){
				System.out.println(val + " execution starting");
			}else{
				String col1 = row.getCell(1).toString();
				String col2 = row.getCell(2).toString();
				String col3 = row.getCell(3).toString();
				String col4 = row.getCell(4).toString();
				
				System.out.println(col1);
				System.out.println(col2);
				System.out.println(col3);
				System.out.println(col4);
				
				operation.perform(propFile, col1, col2, col3,  col4);
			}
			
			System.out.println("----------------------------");
		}
		

	}

}
