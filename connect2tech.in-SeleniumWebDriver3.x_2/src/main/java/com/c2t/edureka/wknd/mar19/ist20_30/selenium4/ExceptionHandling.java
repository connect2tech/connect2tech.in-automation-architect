package com.c2t.edureka.wknd.mar19.ist20_30.selenium4;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ExceptionHandling {
	public static void main(String[] args) {
			int a = 0;
			int b = 20;
			
			int c = 0;
			
			System.out.println("c before division="+c);
			
			try{
				c = b/a;
				
				System.out.println("division completed...");
				
			}catch(Exception e1){
				System.out.println("Exception block...");
				System.out.println(e1);
			}
			
			System.out.println("c after division="+c);
	}
}
