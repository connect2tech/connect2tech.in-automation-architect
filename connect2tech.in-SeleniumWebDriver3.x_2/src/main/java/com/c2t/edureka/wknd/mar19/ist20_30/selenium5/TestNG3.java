package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestNG3 {

	

	@Test(groups={"group2"})
	public void launchBrowser() {
		System.out.println("launchBrowser");
	}
	
	@Test(groups={"group1"})
	public void enterUserName() {
		System.out.println("EnterUserName");
	}
	
	@Test(groups={"group1","group2"})
	public void enterUserPassword() {
		System.out.println("EnterUserPassword");
	}
	
	@Test
	public void clickLogin() {
		System.out.println("clickLogin");
	}

}
