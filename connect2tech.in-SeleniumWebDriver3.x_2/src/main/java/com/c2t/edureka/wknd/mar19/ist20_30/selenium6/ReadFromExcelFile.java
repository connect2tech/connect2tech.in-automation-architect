package com.c2t.edureka.wknd.mar19.ist20_30.selenium6;

import java.awt.print.Printable;
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

// FileInputStream file = new FileInputStream(new
// File("howtodoinjava_demo.xlsx"));
// XSSFWorkbook workbook = new XSSFWorkbook(file);
// XSSFSheet sheet = workbook.getSheetAt(0);
// sheet.getSheetName().equals("Employee Data")
// Iterator<Row> rowIterator = sheet.iterator();
// Row row = rowIterator.next();
// Iterator<Cell> cellIterator = row.cellIterator();

public class ReadFromExcelFile {
	public static void main(String[] args) throws Exception {
		File file = new File("DDT.xlsx");
		FileInputStream fileInputStream = new FileInputStream(file);
		
		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = workbook.getSheet("Employee Data");
		
		Iterator<Row> rows = sheet.rowIterator();
		
		int a = 0;
		
		while(rows.hasNext()){
			
			Row row = rows.next();
			
			if(a == 0)
				continue;
			
			Iterator<Cell> cells = row.cellIterator();
			
			
			while(cells.hasNext()){
			
				
				Cell cellValue = cells.next();
				System.out.println("cellValue="+cellValue);
				
			}
			
			System.out.println("------------------------------------");
			
			
		}
		
	}
}
