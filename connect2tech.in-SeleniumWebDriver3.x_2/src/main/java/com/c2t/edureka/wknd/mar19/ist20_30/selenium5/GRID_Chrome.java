package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.mustache.StringChunk;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class GRID_Chrome {

	WebDriver driver;
	String url ="https://www.facebook.com/";
	String nodeUrl;

	
	@Parameters({"node_url"})
	@BeforeMethod
	public void before(String node) throws Exception{

		
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		//capability.setBrowserName("chrome");
		capability.setPlatform(Platform.WIN10);
		
		WebDriver driver = new RemoteWebDriver(new URL(node), capability);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(url);
	}

	@Test
	public void test1() {
		
	}

}
