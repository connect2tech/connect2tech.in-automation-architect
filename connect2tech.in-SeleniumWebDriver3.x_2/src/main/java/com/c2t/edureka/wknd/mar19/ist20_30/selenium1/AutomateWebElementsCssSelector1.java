package com.c2t.edureka.wknd.mar19.ist20_30.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateWebElementsCssSelector1 {
	public static void main(String[] args) {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/LocatingMultipleElements.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// opened the url in browser
		driver.get(url);

		
		
		//WebElement a = driver.findElement(By.cssSelector("input"));
		//WebElement a = driver.findElement(By.cssSelector("input.w3-input"));
		//WebElement a = driver.findElement(By.cssSelector(".w3-input"));
		//WebElement a = driver.findElement(By.cssSelector("input#fname"));
		//WebElement a = driver.findElement(By.cssSelector("input[type='text'],[value='Testing the value12345']"));
		//WebElement a = driver.findElement(By.cssSelector("input:not[type]"));
		//WebElement a = driver.findElement(By.cssSelector("input[id^='fn']"));
		WebElement a = driver.findElement(By.cssSelector("input[id$='me']"));
		a.clear();
		a.sendKeys("using fn");

	}
}
