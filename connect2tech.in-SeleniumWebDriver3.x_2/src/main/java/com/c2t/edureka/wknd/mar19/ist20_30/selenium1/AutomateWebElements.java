package com.c2t.edureka.wknd.mar19.ist20_30.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateWebElements {
	public static void main(String[] args) {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/LocatingMultipleElements.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// opened the url in browser
		driver.get(url);

		/*WebElement a = driver.findElement(By.id("fname"));
		//web.clear();
		a.sendKeys("shilpi");*/
		
		WebElement a = driver.findElement(By.name("firstname"));
		a.clear();
		a.sendKeys("swati karni");

	}
}
