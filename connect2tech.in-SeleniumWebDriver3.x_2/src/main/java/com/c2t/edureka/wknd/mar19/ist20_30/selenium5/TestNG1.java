package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestNG1 {

	@BeforeTest
	public void before() {
		System.out.println("before");
	}
	
	@BeforeMethod
	public void before_method(){
		System.out.println("before_method");
	}

	@Test
	public void test1() {
		System.out.println("first test");
	}

	@Test
	public void test2() {
		System.out.println("second test");
	}
	
	@AfterMethod
	public void after_method(){
		System.out.println("after_method");
	}

	@AfterTest
	public void after() {
		System.out.println("after");
	}

}
