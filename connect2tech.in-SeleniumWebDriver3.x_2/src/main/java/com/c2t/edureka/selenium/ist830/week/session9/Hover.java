package com.c2t.edureka.selenium.ist830.week.session9;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Hover {

	public static void main(String[] args) {
		String url = "http://newtours.demoaut.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.get(url);

		WebElement row = driver.findElement(
				By.xpath("/html/body/div/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]"));
		String existing_background_color = row.getCssValue("background-color");
		System.out.println("existing_background_color="+existing_background_color);
		Actions action = new Actions(driver);
		action.moveToElement(row).perform();

		String new_background_color = row.getCssValue("background-color");
		System.out.println("new_background_color="+new_background_color);

	}
}