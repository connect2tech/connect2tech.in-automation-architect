package com.c2t.edureka.selenium.ist830.week.session9;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Test_Parallel4 {

	@Test
	public void test2() {
		System.out.println("test2");
		long id3 = Thread.currentThread().getId();
		System.out.println("Test_Parallel/id3=" + id3);

		String url = "https://www.google.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url);
	}

}
