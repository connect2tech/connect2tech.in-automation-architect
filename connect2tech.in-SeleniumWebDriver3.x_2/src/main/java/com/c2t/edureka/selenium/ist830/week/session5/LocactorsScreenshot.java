package com.c2t.edureka.selenium.ist830.week.session5;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocactorsScreenshot {

	public static void main(String[] args) throws IOException {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/LocatingMultipleElements.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver", "D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url);
		
		TakesScreenshot sc = (TakesScreenshot) driver;
		File screenShot = sc.getScreenshotAs((OutputType.FILE));
		FileUtils.copyFile(screenShot, new File("D:/screen"+  ".jpg"));
	
		
	}
}