package com.c2t.edureka.selenium.ist830.week.session9;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Test4 {

	
	@Test(priority = 0)
	public void setSystemProperty() {
		System.out.println("setSystemProperty");
	}
	
	@Test(priority = 1)
	public void openPage1() {
		System.out.println("openPage");
	}
	
	@Test(priority = 1)
	public void openPage2() {
		System.out.println("openPage");
	}

	@Test(priority = 2)
	public void enterId() {
		System.out.println("enterId");
	}

	@Test(priority = 3)
	public void enterPwd() {
		System.out.println("enterPwd");
	}

	@Test(priority = 4)
	public void clickLogin() {
		System.out.println("clickLogin");
	}

}
