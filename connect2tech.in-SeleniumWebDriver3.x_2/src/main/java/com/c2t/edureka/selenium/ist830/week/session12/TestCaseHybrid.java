package com.c2t.edureka.selenium.ist830.week.session12;

import java.util.Properties;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestCaseHybrid {

	WebDriver webdriver;
	ExcelFile excel;
	ReadFromProperties prop;
	Properties p;
	Sheet sheet;
	UIOperations uiOps;

	@BeforeTest
	public void beforeTest() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/connect2tech.in-AutomateBoringStuffInJava/chromedriver_win32_2.45/chromedriver.exe");
		webdriver = new ChromeDriver();
		excel = new ExcelFile();
		sheet = excel.readExcel("D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2",
				"TestCase.xlsx", "KeywordFramework");

		prop = new ReadFromProperties();
		p = prop.readDataFromProperties();

		uiOps = new UIOperations(webdriver);

	}

	@Test
	public void testLogin() throws Exception {

		int rowCount = sheet.getPhysicalNumberOfRows();

		for (int i = 0; i < rowCount; i++) {
			Row row = sheet.getRow(i);

			String s1 = row.getCell(0).toString();
			String s2 = row.getCell(1).toString();
			String s3 = row.getCell(2).toString();
			String s4 = row.getCell(3).toString();
			String s5 = row.getCell(4).toString();
			
			System.out.println(s1);
			System.out.println(s2);
			System.out.println(s3);
			System.out.println(s4);
			System.out.println(s5);
			System.out.println("------------------------------------");
			uiOps.perform(p, s2, s3, s4, s5);
			
		}

	}

}
