package com.c2t.edureka.selenium.ist830.week.session11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PomFB {

	WebDriver d;

	PomFB(WebDriver d1) {
		d = d1;
	}
	
	public void findEmail(String e_mail){
		WebElement we  = d.findElement(By.id("email"));
		we.sendKeys(e_mail);
	}
	
	public void findPassword(String pwd){
		WebElement we  = d.findElement(By.id("pass"));
		we.sendKeys(pwd);
	}
	
	public void clickLogin(){
		WebElement we  = d.findElement(By.id("u_0_2"));
	}

}
