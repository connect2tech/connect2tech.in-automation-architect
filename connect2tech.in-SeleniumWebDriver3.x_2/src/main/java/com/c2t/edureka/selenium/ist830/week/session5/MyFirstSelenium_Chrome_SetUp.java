package com.c2t.edureka.selenium.ist830.week.session5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyFirstSelenium_Chrome_SetUp {

	public static void main(String[] args) {
		
		String url = "https://www.google.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver", "D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.get(url);
	
		WebElement webElement = driver.findElement(By.name("firstname"));
		webElement.sendKeys("hello");
		
	}
}