package com.c2t.edureka.selenium.ist830.week.session9;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MyFirstTestNG1 {

	String url;
	WebDriver driver;
	String expected = "Downloads - ChromeDriver - WebDriver for Chrome";

	@BeforeTest
	public void before() {
		url = "http://chromedriver.chromium.org/downloads";
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void test1() {
		driver.get(url);
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expected,"The expected and actual value of title is not same");
		
	}

	@AfterTest
	public void after() {
		driver.close();
	}

}
