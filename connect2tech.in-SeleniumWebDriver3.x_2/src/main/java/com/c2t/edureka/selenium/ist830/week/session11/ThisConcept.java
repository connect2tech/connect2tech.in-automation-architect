package com.c2t.edureka.selenium.ist830.week.session11;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Person {

	String name;

	public void display() {
		System.out.println(this.name);
		System.out.println("this="+this);
		
	}
}

public class ThisConcept {
	public static void main(String[] args) {
		Person p = new Person();
		p.name = "NC";
		p.display();
		
		System.out.println(p);
		
	}
}
