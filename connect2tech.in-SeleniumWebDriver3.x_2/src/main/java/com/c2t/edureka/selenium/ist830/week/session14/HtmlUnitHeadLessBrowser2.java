package com.c2t.edureka.selenium.ist830.week.session14;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.annotations.Test;

public class HtmlUnitHeadLessBrowser2 {

	@Test
	public void test1() {
		
		WebDriver driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.get("https://www.facebook.com/");
		WebElement we1 = driver.findElement(By.name("email"));
		we1.sendKeys("naresh.javapro@gmail.com");
		
		WebElement we2 = driver.findElement(By.name("pass"));
		we2.sendKeys("Hello12#$");
		
		driver.findElement(By.id("loginbutton")).click();

		
		String title = driver.getTitle();
		System.out.println(title);
		
		
		
		
		
	}

}
