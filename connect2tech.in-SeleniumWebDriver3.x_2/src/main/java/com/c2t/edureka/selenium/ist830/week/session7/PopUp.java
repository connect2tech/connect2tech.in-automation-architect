package com.c2t.edureka.selenium.ist830.week.session7;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PopUp {

	public static void main(String[] args) {
		String url = "http://demo.guru99.com/popup.php";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver", "D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/chromedriver_win32_2.46/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
		
		driver.findElement(By.linkText("Click Here")).click();
		
		String parent = driver.getWindowHandle();
		Set<String> windows =  driver.getWindowHandles();
		
		Iterator <String> iterator = windows.iterator();
		
		while(iterator.hasNext()){
			
			String temp = iterator.next();
			
			if(!temp.equals(parent)){
				driver.switchTo().window(temp);
				driver.findElement(By.name("emailid")).sendKeys("welcome");
			}
			
		}
		
		
	}
}