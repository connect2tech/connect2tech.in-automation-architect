package com.c2t.edureka.selenium.ist830.week.session9;

import java.io.File;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DDT {

	WebDriver d;

	@DataProvider
	public String[][] data() {
		
		String values[][] = new String[4][2];

		try {
			File f = new File("D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/DDT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(f);

			XSSFSheet sheet = workbook.getSheet("Employee Data");

			int rowCount = sheet.getPhysicalNumberOfRows();
			System.out.println(rowCount);

			for (int i = 0; i < rowCount; i++) {
				Row row = sheet.getRow(i);

				int cellCount = row.getPhysicalNumberOfCells();

				for (int j = 0; j < cellCount; j++) {
					Cell c = row.getCell(j);
					System.out.println(c.toString());
					
					values[i][j] = c.toString();
				}

				System.out.println("----------------------------");

			}

			/*
			 * Iterator<Row> rows = sheet.rowIterator();
			 * 
			 * while(rows.hasNext()){ Row row = rows.next();
			 * 
			 * Iterator<Cell> cells = row.cellIterator();
			 * 
			 * while(cells.hasNext()){ Cell cell = cells.next();
			 * System.out.println(cell.toString()); }
			 * 
			 * System.out.println("-------------------");
			 * 
			 * }
			 */

		} catch (Exception e) {
			System.out.println(e);
		}


		return values;
	}

	@BeforeMethod
	public void before() {
		//d = new ChromeDriver();
	}

	@Test(dataProvider = "data")
	public void registerToYahoo(String val1, String val2) {
		

	}

}
