package com.c2t.edureka.selenium.ist830.week.session11;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class TestNgTc {

	WebDriver driver;
	PomFB pom;

	@BeforeTest
	public void before() {
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");

		pom = new PomFB(driver);
	}

	@Test
	public void loginToFb() {
		pom.findEmail("email");
		pom.findPassword("pwd");
		pom.clickLogin();
	}

}
