package com.c2t.edureka.selenium.ist830.week.session11;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PomFB_PageFactory {

	@FindBy(id = "mail")
	WebElement we1;

	@FindBy(id = "pass")
	WebElement passId;

	@FindBy(xpath = "//*[@id='email']")
	WebElement login;

	PomFB_PageFactory(WebDriver d1) {

		PageFactory.initElements(d1, this);
	}

	public void findEmail(String e_mail) {
		we1.sendKeys("abc");
	}

	public void findPassword(String pwd) {
	}

	public void clickLogin() {
	}

}
