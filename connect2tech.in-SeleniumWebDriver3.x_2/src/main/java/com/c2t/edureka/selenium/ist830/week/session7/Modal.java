package com.c2t.edureka.selenium.ist830.week.session7;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Modal {

	public static void main(String[] args) {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/java/com/c2t/edureka/selenium/ist830/week/session7/Modal.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver", "D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		
		driver.get(url);
		
		driver.findElement(By.id("myBtn")).click();
		
		String s = driver.findElement(By.id("myModal")).getText();
		System.out.println(s);
		
		driver.findElement(By.id("myModal")).click();
		
	}
}