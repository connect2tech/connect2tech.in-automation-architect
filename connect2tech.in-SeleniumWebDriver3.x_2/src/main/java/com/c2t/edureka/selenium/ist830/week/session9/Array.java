package com.c2t.edureka.selenium.ist830.week.session9;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Array {
	public static void main(String[] args) {
		int arr [] = {10,20,30,40,50};
		
		int arr_2d [][] = {
				
				{10,20},
				{30,40}
		};
		
	}
}
