package com.c2t.edureka.selenium.ist830.week.session9;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MyFirstTestNG3 {

	String url;
	WebDriver driver;
	String expected = "Downloads - ChromeDriver - WebDriver for Chrome";

	@BeforeTest
	public void beforeTest() {
		System.out.println("beforeTest");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforeMethod");
	}

	@Test
	public void test1() {
		System.out.println("test1");
	}

	@Test
	public void test2() {
		System.out.println("test2");
		throw new RuntimeException();
	}

	@AfterTest
	public void afterTest() {
		System.out.println("AfterTest");

	}

	@AfterMethod
	public void AfterMethod() {
		System.out.println("AfterMethod");
	}

}
