package com.c2t.edureka.selenium.ist830.week.session12;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ReadFromProperties {
	public Properties readDataFromProperties() {
		
		Properties p=null; 

		try {
			File f = new File(
					"D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/java/com/c2t/edureka/selenium/ist830/week/session12/edureka.properties");
			InputStream is = new FileInputStream(f);

			p = new Properties();
			p.load(is);

			System.out.println(p);

			

		} catch (Exception e) {
			System.out.println(e);
		}
		
		return p;

	}
}
